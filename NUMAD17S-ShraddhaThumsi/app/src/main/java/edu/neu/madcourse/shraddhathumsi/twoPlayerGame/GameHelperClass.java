package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

/**
 * Created by shraddha on 3/28/17.
 */

public class GameHelperClass {
    GameStarterAssign gameStarterAssign;
    String[] wordsForThisGame;
    int[] indexForWord;
    String emailOfPlayerA, emailOfPlayerB, winnerOfThisGameByEmail;
    GameHelperClass(GameStarterAssign gameStarterAssign1)
    {
        this.gameStarterAssign = gameStarterAssign1;
    }

    public void setWordPerIndex(String[] wordsForThisGame)
    {
        this.wordsForThisGame = wordsForThisGame;
    }

    public String[] getWordsForThisGame()
    {
        return this.wordsForThisGame;
    }

    public void setIndexForWord(int[] indexForWord)
    {
        this.indexForWord = indexForWord;
    }

    public int[] getIndexForWord()
    {
        return this.indexForWord;
    }

    public  void setEmailOfPlayerA(String emailOfPlayerA)
    {
        this.emailOfPlayerA = emailOfPlayerA;
    }

    public String getEmailOfPlayerA()
    {
        return this.emailOfPlayerA;
    }

    public void setEmailOfPlayerB(String emailOfPlayerB)
    {
        this.emailOfPlayerB = emailOfPlayerB;
    }
    public String getEmailOfPlayerB()
    {
        return this.emailOfPlayerB;
    }
    public void setWinnerOfThisGameByEmail(String winnerOfThisGameByEmail)
    {
        this.winnerOfThisGameByEmail = winnerOfThisGameByEmail;
    }

    public String getWinnerOfThisGameByEmail()
    {
        return this.winnerOfThisGameByEmail;
    }

}
