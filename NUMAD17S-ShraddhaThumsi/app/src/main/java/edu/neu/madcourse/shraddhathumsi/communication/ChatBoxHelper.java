package edu.neu.madcourse.shraddhathumsi.communication;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by shraddha on 3/15/17.
 */

public class ChatBoxHelper {

    ChatBox cbx;
    ViewAllPlayers viewAllPlayers;
    DataSnapshot dataSnapshot;
    User user;

    ChatBoxHelper(ChatBox cbx)
    {
        this.cbx = cbx;
    }

    ChatBoxHelper(ViewAllPlayers viewAllPlayers)
    {
        this.viewAllPlayers =viewAllPlayers;
    }

    public void setDataSnapshot(DataSnapshot dataSnapshot)
    {
        this.dataSnapshot = dataSnapshot;
    }

    public DataSnapshot getDataSnapshot()
    {
        return this.dataSnapshot;
    }
}
