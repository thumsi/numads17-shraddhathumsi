package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import edu.neu.madcourse.shraddhathumsi.R;

import android.content.Intent;
public class TwoPlayerWordGame extends AppCompatActivity {
    Button onePlayerWordGame, twoPlayerWordGame, creditsButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player_word_game);
        onePlayerWordGame = (Button) findViewById(R.id.onePlayerWordGame);
        onePlayerWordGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TwoPlayerWordGame.this, OnePlayerAssign.class);
                startActivity(intent);
            }
        });

        twoPlayerWordGame = (Button) findViewById(R.id.twoPlayerWordGame);
        twoPlayerWordGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TwoPlayerWordGame.this, TwoPlayerAssign.class);
                startActivity(intent);
            }
        });

        creditsButton = (Button) findViewById(R.id.acknowledgements);
        creditsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TwoPlayerWordGame.this, AcknowledgementsTwoPlayer.class);
                startActivity(intent);
            }
        });
    }

}

//credits
// rounded corners for buttons: http://bit.ly/2mOL1sM
// GPS code: http://bit.ly/2n8wXgk
// Date and time: http://bit.ly/2mr2BHn
// bubble sort on strings: http://bit.ly/2mT4KaU, http://bit.ly/2nURzah
// kill an android activity: http://bit.ly/2mSQzCV
// bug in sequence of calls in set Content view and database instance: http://bit.ly/2nEBbh2
// getting a particular key in firebase given child body: http://bit.ly/2nblQBS
// simple examples of Java enum: http://bit.ly/2mwTYLA
// solution for window leak error: http://bit.ly/2mVJZKW
// notification launches to a particular activity: http://bit.ly/2nocMvg