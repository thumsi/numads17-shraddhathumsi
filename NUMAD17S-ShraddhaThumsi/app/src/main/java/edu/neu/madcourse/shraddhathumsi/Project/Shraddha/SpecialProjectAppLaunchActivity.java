package edu.neu.madcourse.shraddhathumsi.Project.Shraddha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import edu.neu.madcourse.shraddhathumsi.R;
public class SpecialProjectAppLaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_project_app_launch);
    }

    public void loadAboutOurTeamToast() {

    }

    public void loadGroupAcknowledgements() {

    }

    public void loadSetSoundProfileHome() {

    }
}
