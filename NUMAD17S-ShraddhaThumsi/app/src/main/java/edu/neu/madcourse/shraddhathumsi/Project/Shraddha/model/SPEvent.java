package edu.neu.madcourse.shraddhathumsi.Project.Shraddha.model;

/**
 * Created by manusaxena on 4/22/17.
 */
public class SPEvent {

    private String date;
    private String startTime;

    public String getStartTimeLong() {
        return startTimeLong;
    }

    public void setStartTimeLong(String startTimeLong) {
        this.startTimeLong = startTimeLong;
    }

    public String getEndTimeLong() {
        return endTimeLong;
    }

    public void setEndTimeLong(String endTimeLong) {
        this.endTimeLong = endTimeLong;
    }

    private String startTimeLong;
    private String endTimeLong;
    private String endTime;
    private String location;
    private String title;
    private String description;
    private boolean silent;
    private boolean vibrate;
    private boolean setVolumeActive;
    private int ringingVolume;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;


    public boolean isSetVolumeActive() {
        return setVolumeActive;
    }

    public void setSetVolumeActive(boolean setVolumeActive) {
        this.setVolumeActive = setVolumeActive;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public boolean isVibrate() {
        return vibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.vibrate = vibrate;
    }

    public int getRingingVolume() {
        return ringingVolume;
    }

    public void setRingingVolume(int ringingVolume) {
        this.ringingVolume = ringingVolume;
    }

    public String getCurrentSoundProfile()
    {
        if(!setVolumeActive)
        {
            return  "Default";
        }
        else if(silent && vibrate)
        {
            return "Silent+Vibrate";
        }
        else if(silent)
        {
            return "Silent";
        }
        else if(vibrate)
        {
            return "Vibrate";
        }
        else
        {
            return "Ringing";
        }
    }



}
