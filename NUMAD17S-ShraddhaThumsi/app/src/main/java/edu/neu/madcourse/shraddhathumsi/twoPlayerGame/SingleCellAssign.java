package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

/**
 * Created by shraddha on 2/21/17.
 */
import android.view.View;

import edu.neu.madcourse.shraddhathumsi.scroggle.ScroggleGameStarter;

public class SingleCellAssign {
    boolean isCellSelected;
    String textOnCell;
    String cellBgColor;
    int individualCellNo;
    OnePlayerGameStarter onePlayerGameStarter;
    GameStarterAssign sgs;
    View fullView;
    SingleCellAssign[] sca;
    SingleCellAssign(GameStarterAssign s)
    {
        this.sgs = s;
    }
    SingleCellAssign(OnePlayerGameStarter onePlayerGameStarter)
    {
        this.onePlayerGameStarter =onePlayerGameStarter;
    }


    void setSelected(boolean selectedValue)
    {
        this.isCellSelected = selectedValue;
    }

    boolean getSelected()
    {
        return this.isCellSelected;
    }

    void setTextOnCell(char t)
    {
        String tex = Character.toString(t);
        this.textOnCell = tex;
    }

    String getTextOnCell()
    {
        return this.textOnCell;
    }

    void setBgColor(String color)
    {
        this.cellBgColor = color;
    }

    String getBgColor()
    {
        return this.cellBgColor;
    }

    void setFullView(View view)
    {
        this.fullView = view;
    }

    View getFullView()
    {
        return this.fullView;
    }

    void setSubTiles(SingleCellAssign[] cellArray)
    {
        this.sca = cellArray;
    }

    SingleCellAssign[] getSubTiles()
    {
        return this.sca;
    }


}