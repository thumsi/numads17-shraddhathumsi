package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import edu.neu.madcourse.shraddhathumsi.R;

import android.view.View;
import android.widget.Button;
import android.content.Intent;
public class OnePlayerAssign extends AppCompatActivity {

    Button startOnePlayerGame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_player);
        startOnePlayerGame = (Button) findViewById(R.id.startOnePlayerGame);
        startOnePlayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnePlayerAssign.this, OnePlayerGameStarter.class);
                startActivity(intent);
            }
        });
    }
}