package edu.neu.madcourse.shraddhathumsi.Project.Shraddha;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.api.client.util.DateTime;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import edu.neu.madcourse.shraddhathumsi.Project.Shraddha.model.SPEvent;
import edu.neu.madcourse.shraddhathumsi.R;

public class LocationService extends Service {

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private GoogleApiClient mGoogleApiClientLocation;
    private static DatabaseReference mDatabase;
    public static String clientToken;
    public static Long now;
    public Geocoder geocoder;
    protected Location mLastLocation  ;
    public static int defvolume=0;
    public static boolean defIsSilent = false;
    public static boolean defIsVibrate = false;
    private ArrayList<SPEvent> allEvents= new ArrayList<SPEvent>();
    Location nLastLocation= new Location("");
    public LocationService() {
    }


    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        geocoder = new Geocoder(this, Locale.getDefault());
        mDatabase = FirebaseDatabase.getInstance().getReference();
        clientToken= FirebaseInstanceId.getInstance().getToken();
        setNow();
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }




    public void setNow()
    {
        mDatabase.child("events/"+clientToken).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        now = new DateTime(System.currentTimeMillis()).getValue();

                        if(dataSnapshot.exists())
                        {
                            for(DataSnapshot child : dataSnapshot.getChildren())
                            {

                                //SPEvent temp =  child.getValue(SPEvent.class);

                                if(now < Long.parseLong(child.getKey()))
                                {
                                    now = Long.parseLong(child.getKey());
                                    break;
                                }
                            }


                        }

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void NotifyUser(String loc, String from , String to)
    {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Your Sound Profile will change in 5 mins")
                        .setContentText("\n@"+loc+"\nFrom:"+from+" To "+to+"\nClick on this notification to edit");
        Intent resultIntent = new Intent(this, EditSoundProfile.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(EditSoundProfile.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(50692, mBuilder.build());
        try
        {

            Long FIVEMIN = 0L;
            FIVEMIN= FIVEMIN+5*60*1000;
            Thread.sleep(5000);
            System.out.println("before location api connect-----------------9");
            System.out.println("before location api connect-----------------10");
        }catch (Exception ex)
        {

        }

    }



    public void changeSound(int volume, boolean isSilent, boolean isVibrate)
    {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        System.out.println("before location api connect-----------------11");
        if(audioManager.getRingerMode()==AudioManager.RINGER_MODE_SILENT)
        {
            defIsSilent=true;
            defvolume=0;
        }
        else
        {
            defIsSilent=false;

            defvolume=audioManager.getStreamVolume(AudioManager.STREAM_RING);
        }
        System.out.println("before location api connect-----------------12");

        if(audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER)==AudioManager.VIBRATE_SETTING_ON)
        {
            defIsVibrate= true;
        }
        else
        {
            defIsVibrate= false;
        }
        System.out.println("before location api connect-----------------13");


        if(isSilent)
        {
            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        }
        else
        {
            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        }

        System.out.println("before location api connect-----------------14");
        if(isVibrate)
        {
            audioManager.setVibrateSetting (AudioManager.VIBRATE_TYPE_RINGER,AudioManager.VIBRATE_SETTING_ON);
        }
        else
        {
            audioManager.setVibrateSetting (AudioManager.VIBRATE_TYPE_RINGER,AudioManager.VIBRATE_SETTING_OFF);
        }

        System.out.println("before location api connect-----------------15");

        audioManager.setStreamVolume(AudioManager.STREAM_RING,audioManager.getStreamMaxVolume(AudioManager.STREAM_RING),volume);

        System.out.println("before location api connect-----------------16");

    }

    public void changeToDefault()
    {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        if(defIsSilent)
        {
            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        }
        else
        {
            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        }


        if(defIsVibrate)
        {
            audioManager.setVibrateSetting (AudioManager.VIBRATE_TYPE_RINGER,AudioManager.VIBRATE_SETTING_ON);
        }
        else
        {
            audioManager.setVibrateSetting (AudioManager.VIBRATE_TYPE_RINGER,AudioManager.VIBRATE_SETTING_OFF);
        }

        System.out.println("before location api connect-----------------17");

        audioManager.setStreamVolume(AudioManager.STREAM_RING,audioManager.getStreamMaxVolume(AudioManager.STREAM_RING),defvolume);

    }





    private final class ServiceHandler extends Handler implements
            GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
    {
        public ServiceHandler(Looper looper) {
            super(looper);
            mDatabase = FirebaseDatabase.getInstance().getReference();
            clientToken= FirebaseInstanceId.getInstance().getToken();
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            Long FIVEMIN = 0L;
            FIVEMIN= FIVEMIN+5*60*1000;
            while(true)
            {
                try {
                    Thread.sleep(60000);
                    long currenttime = System.currentTimeMillis();
                    buildGoogleApiClient();
                    System.out.println(currenttime);
                    System.out.println(now);

                    setNow();
                    if((currenttime > (now- (FIVEMIN)))&&(currenttime < now))
                    {
                        mGoogleApiClientLocation.connect(); // go inside if
                    }


                    System.out.println("before location api connect-----------------0");


                    System.out.println("before location api connect-----------------1");

                } catch (InterruptedException e) {
                    // Restore interrupt status.
                    Thread.currentThread().interrupt();
                }
            }

        }



        @Override
        public void onConnected(Bundle connectionHint) {
            // Provides a simple way of getting a device's location and is well suited for
            // applications that do not require a fine-grained location and that do not need location
            // updates. Gets the best and most recent location currently available, which may be null
            // in rare cases when a location is not available.

            try
            {
                System.out.println("before location api connect ------------2");
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClientLocation);
                //System.out.println("before location api connect ------------3"+ mLastLocation.toString());
                if(mLastLocation!=null)
                {

                    mDatabase.child("events/"+clientToken).child(now.toString()).addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists())
                                    {
                                        System.out.println("before location api connect ------------4");
                                        SPEvent temp =  dataSnapshot.getValue(SPEvent.class);
                                        try
                                        {
                                            List<Address> adrs = geocoder.getFromLocationName(temp.getLocation(),1);
                                            System.out.println("before location api connect ------------5"+adrs.get(0).getLocality().toString());
                                            Address adr = adrs.get(0);
                                            nLastLocation.setLatitude(adr.getLatitude());
                                            nLastLocation.setLongitude(adr.getLongitude());

                                            System.out.println("before location api connect ------------6"+ mLastLocation.toString());
                                            System.out.println("before location api connect ------------7"+ nLastLocation.toString());
                                            if(mLastLocation.distanceTo(nLastLocation)<100)
                                            {
                                                System.out.println("before location api connect ------------8");
                                                NotifyUser(temp.getLocation(),temp.getStartTime(),temp.getEndTime());
                                                changeSound(temp.getRingingVolume(),temp.isSilent(),temp.isVibrate());
                                                setNow();
                                                //Thread.sleep(Long.parseLong(temp.getEndTimeLong())-Long.parseLong(temp.getStartTimeLong()));
                                                //changeToDefault();
                                                mGoogleApiClientLocation.disconnect();
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            System.out.println(ex);

                                        }

                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                }

                else
                {
                    mDatabase.child("events/"+clientToken).child(now.toString()).addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists())
                                    {
                                        System.out.println("before location api connect ------------4");
                                        SPEvent temp =  dataSnapshot.getValue(SPEvent.class);
                                        NotifyUser("Location not detected,Just a reminder for your event@"+temp.getLocation(),temp.getStartTime(),temp.getEndTime());

                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                }



            }
            catch (SecurityException ex)
            {
                System.out.println(ex+" exception for location code");
            }


            setNow();

        }

        @Override
        public void onConnectionFailed(ConnectionResult result) {
            // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
            // onConnectionFailed.
            Log.i("LocationService", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
        }


        @Override
        public void onConnectionSuspended(int cause) {
            // The connection to Google Play services was lost for some reason. We call connect() to
            // attempt to re-establish the connection.
            Log.i("LocationService", "Connection suspended");
            mGoogleApiClientLocation.connect();
        }


        protected synchronized void buildGoogleApiClient() {
            mGoogleApiClientLocation = new GoogleApiClient.Builder(getApplicationContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }




}
