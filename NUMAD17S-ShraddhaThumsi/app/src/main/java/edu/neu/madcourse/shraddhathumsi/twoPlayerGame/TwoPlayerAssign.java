package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import edu.neu.madcourse.shraddhathumsi.R;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.view.View;
import java.util.ArrayList;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

public class TwoPlayerAssign extends AppCompatActivity {

    Button locationFinder, loginToAccountButton, createAccountButton;
    EditText userEmailText, userNameText;
    String userNameValue, userEmailValue;
    public static String token = "";
    private static String CLIENT_REGISTRATION_TOKEN = "";

    public static UserAssign user;
    public static UserAssign currentlyLoggedInUser;
    private DatabaseReference mDatabase;
    String initialScore = String.valueOf(0);
    TextView latitudeText, longitudeText, streetName, cityName, addressText, countryName;
    ArrayList<DataSnapshot> listOfUsers = new ArrayList<>();
    ArrayList<UserAssign> users = new ArrayList<>();
    ArrayList<String> emailList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player);
        //currentlyLoggedInUser.setTwoPlayerAssign(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        locationFinder = (Button) findViewById(R.id.locationFinder);
        loginToAccountButton = (Button) findViewById(R.id.loginToAccount);
        createAccountButton = (Button) findViewById(R.id.createAccount);
        final GPSTrackerClass gpsTracker = new GPSTrackerClass(this);
        latitudeText = (TextView) findViewById(R.id.latitude);
        longitudeText = (TextView) findViewById(R.id.longitude);
        streetName = (TextView) findViewById(R.id.streetName);
        cityName = (TextView) findViewById(R.id.cityName);
        addressText = (TextView) findViewById(R.id.address);
        countryName = (TextView) findViewById(R.id.countryName);
        userEmailText = (EditText) findViewById(R.id.userEmail);
        userNameText = (EditText) findViewById(R.id.userName);
        locationFinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gpsTracker.getIsGPSTrackingEnabled())
                {
                    String country = gpsTracker.getCountryName(v.getContext());
                    String city = gpsTracker.getLocality(v.getContext());
                    double latitude = gpsTracker.getLatitude();
                    double longitude = gpsTracker.getLongitude();
                    String address = gpsTracker.getAddressLine(v.getContext());
                    countryName.setText(country);
                    cityName.setText(city);
                    latitudeText.setText(String.valueOf(latitude));
                    longitudeText.setText(String.valueOf(longitude));
                    addressText.setText(address);

                }
                else
                {
                    Toast.makeText(TwoPlayerAssign.this, "You will need to enable your GPS to detect where you are", Toast.LENGTH_SHORT).show();
                }


            }
        });

        userEmailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userEmailValue = String.valueOf(s);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        userNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userNameValue = String.valueOf(s);

            }

            @Override
            public void afterTextChanged(Editable s) {

                userNameValue = String.valueOf(s);
            }
        });

        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot d: dataSnapshot.getChildren())
                {
                    listOfUsers.add(d);
                    users.add(d.getValue(UserAssign.class));
                    emailList.add(d.getValue(UserAssign.class).getEmail());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        loginToAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable(v))
                {
                    token = FirebaseInstanceId.getInstance().getToken();
                    CLIENT_REGISTRATION_TOKEN = token;
                    String country = gpsTracker.getCountryName(v.getContext());
                    String city = gpsTracker.getLocality(v.getContext());
                    double latitude = gpsTracker.getLatitude();
                    double longitude = gpsTracker.getLongitude();
                    String address = gpsTracker.getAddressLine(v.getContext());
                    if(gpsTracker.getIsGPSTrackingEnabled())
                    {

                        countryName.setText(country);
                        //cityName.setText(city + " city");
                        latitudeText.setText(String.valueOf(latitude));
                        longitudeText.setText(String.valueOf(longitude));
                        //addressText.setText(address);

                    }
                    else
                    {
                        Toast.makeText(TwoPlayerAssign.this, "You will need to enable your GPS to detect where you are", Toast.LENGTH_SHORT).show();
                    }
                    user = new UserAssign(userNameValue, initialScore, userEmailValue, token, city, country);
                    if(emailList.contains(user.getEmail()))
                    {
                        currentlyLoggedInUser = user;
                        String key = user.getKey();
                        Intent intent = new Intent(TwoPlayerAssign.this, ViewAllPlayersAssign.class);
                        intent.putExtra("keyOfLoggedIn", key);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(TwoPlayerAssign.this, "We cannot recognize you, please sign up first", Toast.LENGTH_SHORT).show();

                    }


                }
                else
                {
                    Toast.makeText(TwoPlayerAssign.this, "There seems to be an internet issue, please try again later", Toast.LENGTH_SHORT).show();


                }
            }
        });

        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable(v))
                {
                    String country = gpsTracker.getCountryName(v.getContext());
                    String city = gpsTracker.getLocality(v.getContext());
                    double latitude = gpsTracker.getLatitude();
                    double longitude = gpsTracker.getLongitude();
                    String address = gpsTracker.getAddressLine(v.getContext());
                    if(gpsTracker.getIsGPSTrackingEnabled())
                    {

                        countryName.setText(country);
                        cityName.setText(city + " city");
                        latitudeText.setText(String.valueOf(latitude));
                        longitudeText.setText(String.valueOf(longitude));
                        addressText.setText(address);

                    }
                    else
                    {
                        Toast.makeText(TwoPlayerAssign.this, "You will need to enable your GPS to detect where you are", Toast.LENGTH_SHORT).show();
                    }
                    user = new UserAssign(userNameValue, initialScore, userEmailValue, token, city, country);
                    if(!emailList.contains(user.getEmail()))
                    {
                        currentlyLoggedInUser = user;
                        mDatabase.child("users").push().setValue(user);
                        mDatabase.child("users")
                                .orderByChild("email")
                                .equalTo(user.getEmail())
                                .addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        String userKey = dataSnapshot.getKey();
                                        user.setKey(userKey);
                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        String userKey = dataSnapshot.getKey();
                                        user.setKey(userKey);
                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                        Intent intent = new Intent(TwoPlayerAssign.this, ViewAllPlayersAssign.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(TwoPlayerAssign.this, "We already have your record, please try to sign in.", Toast.LENGTH_SHORT).show();

                    }

                }

                else
                {
                    Toast.makeText(TwoPlayerAssign.this, "There seems to be an internet issue, please try again later", Toast.LENGTH_SHORT).show();

                }


            }
        });


    }

    private boolean isNetworkAvailable(View v) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(v.getContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}