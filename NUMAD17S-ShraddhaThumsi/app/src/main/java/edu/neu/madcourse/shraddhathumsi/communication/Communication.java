package edu.neu.madcourse.shraddhathumsi.communication;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import edu.neu.madcourse.shraddhathumsi.R;
import android.widget.EditText;

public class Communication extends AppCompatActivity {

    private static final String TAG = Communication.class.getSimpleName();

    // Please add the server key from your firebase console in the follwoing format "key=<serverKey>"
    private static final String SERVER_KEY = "key=AAAA6SlZN3A:APA91bGMHCjuFeA36FL2yx3ScichTGUKxOnacIR-HkqTgHts-qqYoRLV8sTyNPQZj17s370vUMul5gjOYp7DBtFDQAcIKLQEflAlhDBR0Qop8xBlzackdA4LnFrluFgll1Q1ypUNhQkd";
    // This is the client registration token
    private static String CLIENT_REGISTRATION_TOKEN = "";
    //private static final String CLIENT_REGISTRATION_TOKEN = "ce7MJ1Z53L8:APA91bHCtvgmTvuvF3KEXPZUSkat-39vZseDGBAFIdZOra5k77kH4p9hDiTCN2cG_ZVvrlpHFP4IiF_FyekKff7_eP2V8W6v38f8H3HvUyWum3yRJtXkkwd2tCZiShFc4Lt3wMfE7mhO";
   EditText userEmail, userName;
    String name = "", email = "";
    String initialScore = String.valueOf(0);
    Button credits, sendNotification;
    public static User user;
    private DatabaseReference mDatabase;
    public static String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fcm);
        userName = (EditText) findViewById(R.id.userName);
        userEmail = (EditText) findViewById(R.id.userEmail);
        credits = (Button) findViewById(R.id.creditsButton);
        sendNotification = (Button) findViewById(R.id.sendNotification);
        user = new User(this);


        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        userEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                email = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Button logTokenButton = (Button) findViewById(R.id.logTokenButton);
        logTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get token
                if(isNetworkAvailable(v))
                {
                    Toast.makeText(Communication.this, "You are online!", Toast.LENGTH_SHORT).show();
                }

                else
                {

                    Toast.makeText(Communication.this, "You are offline but you can still play", Toast.LENGTH_SHORT).show();
                }
                token = FirebaseInstanceId.getInstance().getToken();
                CLIENT_REGISTRATION_TOKEN = token;

                user = new User(name, initialScore, email, token);


                mDatabase.child("users").push().setValue(user);

                String msg = getString(R.string.msg_token_fmt, token);
                Log.d(TAG, msg);
                Toast.makeText(Communication.this, msg, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), ViewAllPlayers.class);
                startActivity(intent);
            }
        });

        Button viewAllPlayers = (Button) findViewById(R.id.viewAllPlayers);
        viewAllPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ViewAllPlayers.class);
                startActivity(intent);
            }
        });

        sendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NotifyAllPlayers.class);
                startActivity(intent);
            }
        });

        credits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AcknowledgementsCommunication.class);
                startActivity(intent);
            }
        });
    }

    private boolean isNetworkAvailable(View v) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(v.getContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


}


//credits
// edit text listener: http://bit.ly/2kp0YIX
// data retrieval from firebase: http://bit.ly/2nyEt1E
//clickable list View: http://bit.ly/2mH9oLz
// internet availability code: http://bit.ly/2mpBVCG

