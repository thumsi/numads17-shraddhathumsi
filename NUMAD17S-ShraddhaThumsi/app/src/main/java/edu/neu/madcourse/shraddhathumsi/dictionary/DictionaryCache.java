package edu.neu.madcourse.shraddhathumsi.dictionary;

/**
 * Created by shraddha on 2/1/17.
 */

import java.util.Hashtable;

public class DictionaryCache {

    //here we will implement list of list so that words are not searched repeatedly
    private static DictionaryCache dc = null;
    //private int NO_OF_WORDS_PER_SEARCH = 100;
    Hashtable<String, String[]> hash = new Hashtable();
   // String[] wordList = new String[NO_OF_WORDS_PER_SEARCH];

    DictionaryCache()
    {
    }



    public static DictionaryCache dcMethod()
    {
        if(dc == null)
        {
            dc = new DictionaryCache();
        }

        return dc;
    }

}
