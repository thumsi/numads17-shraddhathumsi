package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import edu.neu.madcourse.shraddhathumsi.R;
import edu.neu.madcourse.shraddhathumsi.communication.CreateNotification;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.app.Notification;
import android.app.NotificationManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
public class CreateNotificationAssign extends AppCompatActivity {

    private static final String TAG = CreateNotificationAssign.class.getSimpleName();
    private static final String SERVER_KEY = "key=AAAA6SlZN3A:APA91bGMHCjuFeA36FL2yx3ScichTGUKxOnacIR-HkqTgHts-qqYoRLV8sTyNPQZj17s370vUMul5gjOYp7DBtFDQAcIKLQEflAlhDBR0Qop8xBlzackdA4LnFrluFgll1Q1ypUNhQkd";
    // This is the client registration token
    private static String CLIENT_REGISTRATION_TOKEN = "";
    public String token = "";
    EditText message;
    String name = "", messageValue = "";
    String clientToken, key, userName, email, score;
    //key username email score
    static String loggedInKey;
    Button sendInvite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notification_assign);
        Intent intent=getIntent();
        clientToken=intent.getStringExtra("clienttoken");
        System.out.println(clientToken + " client token as in chat box");
        key = intent.getStringExtra("key");
        System.out.println(key + " key as in chat box");
        userName = intent.getStringExtra("userName");
        System.out.println(userName + " user name as in chat box");
        email = intent.getStringExtra("email");
        System.out.println(email + " email as in chat box");
        score =intent.getStringExtra("score");
        System.out.println(score + " score as in chat box");
        loggedInKey = intent.getStringExtra("loggedInKey");
        intent.putExtra("clienttoken",clientToken);
        intent.putExtra("email", email);
        intent.putExtra("key", key);
        intent.putExtra("userName", userName);
        intent.putExtra("score", score);

        System.out.println(clientToken + " client token received via shared preference");
        message = (EditText) findViewById(R.id.messageAssign);
        sendInvite = (Button) findViewById(R.id.sendInviteAssign);
        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                messageValue = s.toString();
                System.out.println(messageValue + ": my invite");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        sendInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                token = FirebaseInstanceId.getInstance().getToken();
                CLIENT_REGISTRATION_TOKEN = clientToken;
                System.out.println(CLIENT_REGISTRATION_TOKEN + " token received on invitation");
                pushNotification(v);
                Intent intent = new Intent(CreateNotificationAssign.this, GameStarterAssign.class);
                intent.putExtra("clienttoken",clientToken);
                intent.putExtra("email", email);
                intent.putExtra("key", key);
                intent.putExtra("userName", userName);
                intent.putExtra("score", score);
                intent.putExtra("loggedInKey", loggedInKey);
                startActivity(intent);

            }
        });
    }

    public void pushNotification(View type) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pushNotification();
            }
        }).start();
    }

    public void pushNotification() {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        System.out.println("client token from big push function");
        try {
            jNotification.put("title", "Google I/O 2016");
            jNotification.put("body", "Firebase Cloud Messaging (App)");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "OPEN_ACTIVITY_1");

            // If sending to a single client
            jPayload.put("to", CLIENT_REGISTRATION_TOKEN);

            /*
            // If sending to multiple clients (must be more than 1 and less than 1000)
            JSONArray ja = new JSONArray();
            ja.put(CLIENT_REGISTRATION_TOKEN);
            // Add Other client tokens
            ja.put(FirebaseInstanceId.getInstance().getToken());
            jPayload.put("registration_ids", ja);
            */

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "run: " + resp);
                    Toast.makeText(CreateNotificationAssign.this,resp,Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}