package edu.neu.madcourse.shraddhathumsi.communication;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.shraddhathumsi.R;

public class ChatBox extends AppCompatActivity {
    private DatabaseReference mDatabase;
    static String key = "";
    ArrayList<User> user12 = new ArrayList<>();
    User user2 = null;
    static String score = "", userName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_box);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Button sendButton=(Button)findViewById(R.id.send_button);
        final EditText wordtext=(EditText)findViewById(R.id.sendScore);
        mDatabase.child("users").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        user2 = dataSnapshot.getValue(User.class);
                        System.out.println(user2);
                        key = dataSnapshot.getKey();
                        user2.setScore(String.valueOf(user2.getScore()));
                        user2.setUsername(String.valueOf(user2.username));
                        wordtext.setText("Your current score: " + user2.getScore());
                        score = String.valueOf(user2.getScore());
                        Log.v("TAG", "onChildAdded: "+dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        user2 = dataSnapshot.getValue(User.class);
                        System.out.println(user2);
                        key = dataSnapshot.getKey();
                        user2.setScore(String.valueOf(user2.getScore()));
                        user2.setUsername(String.valueOf(user2.username));
                        wordtext.setText("Your current score: " + user2.getScore());
                        score = String.valueOf(user2.getScore());
                        Log.v("TAG", "onChildChanged: "+dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("TAG", "onCancelled:" + databaseError);
                    }
                }
        );

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //save to database
                if(isNetworkAvailable(v))
                {
                    Toast.makeText(ChatBox.this, "You are online!", Toast.LENGTH_SHORT).show();
                }

                else
                {

                    Toast.makeText(ChatBox.this, "You are offline but you can still play", Toast.LENGTH_SHORT).show();
                }

               // mDatabase.child("users").child(key).child("score").setValue(wordtext.getText().toString());



                ChatBox.this.modifyUserScore(mDatabase, key);


            }
        });
    }


    private boolean isNetworkAvailable(View v) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(v.getContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    private void modifyUserScore(DatabaseReference postRef, String userName)
    {

        Log.e("TAG", "modifyUserScore: " + userName);
        postRef

                .child("users")
                .child(userName)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        User u = mutableData.getValue(User.class);
                        if (u == null) {
                            return Transaction.success(mutableData);
                        }
                        u.score = String.valueOf(score);
                        mutableData.setValue(u);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d("postTran", "postTransaction:onComplete:" + databaseError);
                    }
                });
    }



}