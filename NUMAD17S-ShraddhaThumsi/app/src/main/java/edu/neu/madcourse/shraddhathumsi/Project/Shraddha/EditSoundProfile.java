package edu.neu.madcourse.shraddhathumsi.Project.Shraddha;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import edu.neu.madcourse.shraddhathumsi.R;

public class EditSoundProfile extends AppCompatActivity {

    public static final String KEY_RESTORE = "key_restore";
    private static DatabaseReference mDatabase;
    public static String clientToken;
    private String eventId;
    private boolean silentOn= false;
    private boolean vibrateOn= false;
    private int ringingVolume =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sound_profile);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        clientToken= FirebaseInstanceId.getInstance().getToken();

        eventId = getIntent().getStringExtra(KEY_RESTORE);

        Switch silentSwitch = (Switch)findViewById(R.id.silent);
        silentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setSilentOn(isChecked);
            }
        });

        Switch vibrateSwitch = (Switch)findViewById(R.id.vibrate);
        vibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setVibrateOn(isChecked);
            }
        });


        SeekBar volControl = (SeekBar)findViewById(R.id.ringingVolume);
        volControl.setMax(100);
        volControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
                setRingingVolume(arg1);
            }

        });


        Button save = (Button)findViewById(R.id.saveSettings);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child("events").child(clientToken).child(eventId).child("silent").setValue(isSilentOn());
                mDatabase.child("events").child(clientToken).child(eventId).child("vibrate").setValue(isVibrateOn());
                mDatabase.child("events").child(clientToken).child(eventId).child("ringingVolume").setValue(getRingingVolume());
                onBackPressed();
            }
        });

        Button cancel = (Button)findViewById(R.id.cancelSettings);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button delete = (Button)findViewById(R.id.deleteSoundSettings);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child("events").child(clientToken).child(eventId).removeValue();
                onBackPressed();
            }
        });
    }


    public boolean isSilentOn() {
        return silentOn;
    }

    public void setSilentOn(boolean silentOn) {
        this.silentOn = silentOn;
    }

    public boolean isVibrateOn() {
        return vibrateOn;
    }

    public void setVibrateOn(boolean vibrateOn) {
        this.vibrateOn = vibrateOn;
    }

    public int getRingingVolume() {
        return ringingVolume;
    }

    public void setRingingVolume(int ringingVolume) {
        this.ringingVolume = ringingVolume;
    }
}
