package edu.neu.madcourse.shraddhathumsi.scroggle;

/**
 * Created by shraddha on 2/25/17.
 */

public class PhaseTwoHelperGrid {
    ScrogglePhaseTwo scrogglePhaseTwo;
    public ScroggleGameStarter scroggleGameStarter;
    int WORD_ARRAY_SIZE = 1000;
    String[] words;
    int[] scorePerWord;


    PhaseTwoHelperGrid(ScrogglePhaseTwo spt)
    {
        this.scrogglePhaseTwo = spt;
        words = new String[this.WORD_ARRAY_SIZE];
        scorePerWord = new int[this.WORD_ARRAY_SIZE];
        for(int i = 0; i < this.WORD_ARRAY_SIZE; i++)
        {
            words[i] = "";
            scorePerWord[i] = 0;
        }
    }

    PhaseTwoHelperGrid(ScroggleGameStarter sgs)
    {
        this.scroggleGameStarter = sgs;
    }


}
