package edu.neu.madcourse.shraddhathumsi.dictionary;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

import  android.media.ToneGenerator;
import android.media.AudioManager;

import edu.neu.madcourse.shraddhathumsi.AssignmentOneMainActivity;
import edu.neu.madcourse.shraddhathumsi.R;


public class Dictionary extends AppCompatActivity {
    ArrayList<String> arrayList = new ArrayList<String>();
    EditText editText;
    String searchWord;
    TextView displayWord;
    Button clearText, mainMenu1, acknowledgements;
    Context context;
    String filename = new String();
    int MAXIMUM_SIZE_ARRAY = 100;
    String[] listOfWords = new String[MAXIMUM_SIZE_ARRAY];
    DictionaryCache dc = DictionaryCache.dcMethod();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        context = getApplicationContext();
        mainMenu1 = (Button) findViewById(R.id.mainMenu);
        mainMenu1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(view.getContext(), AssignmentOneMainActivity.class);
                startActivity(intent);
            }
        });

        acknowledgements = (Button) findViewById(R.id.acknowledgements);
        acknowledgements.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(view.getContext(), AcknowledgementsPage.class);
                startActivity(intent);
            }
        });

        displayWord = (TextView) findViewById(R.id.displayWord);
        editText = (EditText) findViewById(R.id.editText);
        searchWord = editText.getText().toString();
        getSupportActionBar().setTitle("Test Dictionary");


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
       /*searchWord=charSequence;*/
                CharSequence filenameChar = charSequence;
                String filename = null;
                String temp = filenameChar.toString();
                int id;



                try{
                if(charSequence.length()>=3) {

                    filename = charSequence.subSequence(0, 3).toString();
                    String[] searchList = dc.hash.get(filename);
                    id = getResources()
                            .getIdentifier(filename, "raw", getPackageName());
                    String returnedFileContent = readFileFromRawDirectory(id);
                    listOfWords = returnedFileContent.split("\\r?\n");
                    System.out.println(listOfWords);

                    dc.hash.put(filename, listOfWords);

                    for(String s: searchList)
                    {
                        if(s.equals(charSequence.toString()))
                        {


                            arrayList.add(s);
                            ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                            toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP,150);

                        }

                    }


                    for(String s: searchList)
                    {
                        if(s.length() > charSequence.toString().length())
                        {
                            arrayList.remove(s);

                        }
                    }




                }


                    else
                {
                  // arrayList.clear();
                }

                    ArrayAdapter<String> ad = new ArrayAdapter<String>(Dictionary.this, R.layout.dummy, arrayList);
                    ListView w = (ListView) findViewById(R.id.wordList);
                    w.setAdapter(ad);

                }
                catch(Exception e)
                {}
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        clearText = (Button) findViewById(R.id.clearText);
        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.getText().clear();
            }
        });


    }


    private String readFileFromRawDirectory(int resourceId) {
        InputStream iStream = context.getResources().openRawResource(resourceId);
        ByteArrayOutputStream byteStream = null;
        try {
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteStream.toString();
    }




}









    //add in acknowledgements:
    // code to retrieve data from edit text: http://bit.ly/2kp9Ga1
    // programmatically set text to text view: http://bit.ly/2k0dvSf
    // edit text listener: http://bit.ly/2kp0YIX
    // clear text code: http://bit.ly/2jnltnB
    // file reading from raw folder: http://bit.ly/2jxOGsN
    // getting id of raw file by file name: http://bit.ly/2jP04DV
    // rendering text view for array adapter: http://bit.ly/2jybKr7
    // implementing hash table: http://bit.ly/2kV0hUr
    // SUBSTRING check: http://bit.ly/2jFle49
    // foreach  guide: http://bit.ly/2kTGNmr
    // beep generator: http://bit.ly/2kYCRB3




