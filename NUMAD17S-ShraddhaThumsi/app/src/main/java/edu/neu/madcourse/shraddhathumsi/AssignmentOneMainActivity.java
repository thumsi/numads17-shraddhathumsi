package edu.neu.madcourse.shraddhathumsi;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.content.ComponentName;

import edu.neu.madcourse.shraddhathumsi.Project.Shraddha.FinalProject;
import edu.neu.madcourse.shraddhathumsi.communication.Communication;
import edu.neu.madcourse.shraddhathumsi.dictionary.Dictionary;
import edu.neu.madcourse.shraddhathumsi.scroggle.WordGame;
import edu.neu.madcourse.shraddhathumsi.twoPlayerGame.TwoPlayerWordGame;
import android.widget.Toast;

public class AssignmentOneMainActivity extends AppCompatActivity {

    Button quitButton, ticTacToeButton, aboutButton, errorButton, dictionaryButton, trickiestPartsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shraddha_assignment_one);
        getSupportActionBar().setTitle("Shraddha Thumsi");

        quitButton = (Button) findViewById(R.id.quitButton);
        quitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);

            }
        });
        ticTacToeButton = (Button) findViewById(R.id.ticTacToeButton);
        ticTacToeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("edu.neu.madcourse.shraddhathumsi", "edu.neu.madcourse.shraddhathumsi.tictactoe.MainActivity"));
                startActivity(intent);
            }
        });


        errorButton = (Button) findViewById(R.id.errorButton);
        errorButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                throw new RuntimeException("Test runtime exception");
            }
        });




    }

    public void finalProject(View view)
    {
        System.out.println("trickiest parts was started");
        Intent intent = new Intent(this, FinalProject.class);
        startActivity(intent);
    }

    public void loadAboutPage(View view)
    {

        System.out.println("about button was pressed");
        Intent intent = new Intent(this, DisplayAboutActivity.class);
        startActivity(intent);
    }

    public void loadDictionary(View view)
    {
        System.out.println("dictionary button was pressed");
        Intent intent = new Intent(this, Dictionary.class);
        startActivity(intent);
    }

    public void loadWordGame(View view)
    {
        System.out.println("word game was started");
        Intent intent = new Intent(this, WordGame.class);
        startActivity(intent);
    }

    public void loadCommunication(View view)
    {
        System.out.println("two player communication was started");
        Intent intent = new Intent(this, Communication.class);
        startActivity(intent);
    }

    public void loadTwoPlayerWordGame(View view)
    {
        System.out.println("two player word game was started");
        Intent intent = new Intent(this, TwoPlayerWordGame.class);
        if(isNetworkAvailable(view))
        {
            Toast.makeText(AssignmentOneMainActivity.this, "You are online!", Toast.LENGTH_SHORT).show();
        }

        else
        {

            Toast.makeText(AssignmentOneMainActivity.this, "You are offline but you can still play", Toast.LENGTH_SHORT).show();
        }
        startActivity(intent);
    }

    private boolean isNetworkAvailable(View v) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(v.getContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
