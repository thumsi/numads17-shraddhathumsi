package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

/**
 * Created by shraddha on 3/1/17.
 */

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

import edu.neu.madcourse.shraddhathumsi.communication.Communication;

/**
 * Created by aniru on 2/18/2017.
 */
//test git configs
@IgnoreExtraProperties
public class UserAssign {

    public String username;
    public String score, cityName, countryName;
    public String token, email;
    public int userPosInArrayList;
    String key;
    public Communication fcm;
    TwoPlayerAssign twoPlayerAssign = new TwoPlayerAssign();
    ArrayList<String> listOfAllUsers = new ArrayList<>();
    ArrayList<DataSnapshot> dataSnapshotArrayList = new ArrayList<>();
    int indexOfWord;
    ArrayList<String> word = new ArrayList<>();
    public GameStarterAssign gameStarterAssign;


    public UserAssign(){
        // Default constructor required for calls to DataSnapshot.getValue(UserAssign.class)
    }
    public UserAssign(String username, String score, String email, String token)
    {
        this.username = username;
        this.score = score;
        this.token = token;
        this.email = email;
    }
    public UserAssign(String username, String score, String email, String token, String cityName, String countryName)
    {
        this.username = username;
        this.score = score;
        this.token = token;
        this.email = email;
        this.cityName = cityName;
        this.countryName = countryName;
    }
    public UserAssign(GameStarterAssign gameStarterAssign)
    {
        this.gameStarterAssign = gameStarterAssign;

    }
    public void setTwoPlayerAssign(TwoPlayerAssign twoPlayerAssign1)
    {
        this.twoPlayerAssign = twoPlayerAssign1;
    }
    public TwoPlayerAssign getTwoPlayerAssign()
    {
        return this.twoPlayerAssign;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setScore(String score)
    {
        this.score = score;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }


    public String getUsername()
    {
        return this.username;
    }

    public String getScore()
    {
        return this.score;
    }

    public String getToken()
    {
        return this.token;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setUserPosInArrayList(int p)
    {
        this.userPosInArrayList = p;
    }

    public int getUserPosInArrayList()
    {

        return this.userPosInArrayList;
    }

    public void setListOfAllUsers(ArrayList<String> listOfAllUsers)
    {
        this.listOfAllUsers = listOfAllUsers;
    }

    public ArrayList<String> getListOfAllUsers()
    {
        return this.listOfAllUsers;
    }

    public void setDataSnapshotArrayList(ArrayList<DataSnapshot> dps)
    {
        this.dataSnapshotArrayList = dps;
    }

    public ArrayList<DataSnapshot> getDataSnapshotArrayList()
    {
        return this.dataSnapshotArrayList;
    }
    public void setKey(String key)
    {

        this.key = key;
        System.out.println(this.key + " : get key for user from user assign setter");
    }

    public void setIndexOfWord(int indexOfWord)
    {
        this.indexOfWord = indexOfWord;
    }

    public int getIndexOfWord()
    {
        return this.indexOfWord;
    }

    public void setWord(String word)
    {
        String localWord = word;
        this.word.add(localWord);
    }

    public ArrayList<String> getWords()
    {
        return this.word;
    }
    public String getKey()
    {
        return this.key;
    }




}