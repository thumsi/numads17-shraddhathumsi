package edu.neu.madcourse.shraddhathumsi.scroggle;
// final debugging for single player
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;
import org.w3c.dom.Text;

import edu.neu.madcourse.shraddhathumsi.R;
import edu.neu.madcourse.shraddhathumsi.dictionary.Dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import android.widget.ArrayAdapter;
public class ScrogglePhaseTwo extends AppCompatActivity {
    CountDownTimer timer;
    //ScroggleGameStarter sgs;
    IndividualGrid ig = new IndividualGrid(this);
    PhaseTwoHelperGrid pgh = new PhaseTwoHelperGrid(this);
    SuperGrid spt = new SuperGrid(this);
    HashMap<Integer, Character> phaseTwoMap = new HashMap<>();
    ArrayList<String> possibleWords = new ArrayList<>();
    ArrayList<String> wordsMadeByUser = new ArrayList<>();
    ArrayList<String> wordsFromLetter = new ArrayList<String>();
    char startingLetter;
    private final int START = 90000, INTERVAL = 1000;
    TextView phaseTwoDisplayTime, phaseTwoDisplayScore;

    int[] cellArrayPhaseTwo = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};
    int GRID_SIZE = cellArrayPhaseTwo.length;
    SingleCell[] smallGrid = new SingleCell[GRID_SIZE];
    String word = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //LayoutInflater factory = LayoutInflater.from(this);
        //final View rootView = factory.inflate(R.layout.activity_scroggle_phase_two, null);
        setContentView(R.layout.activity_scroggle_phase_two);

        startingLetter = spt.selectStartingLetter(phaseTwoMap);
        phaseTwoDisplayTime = (TextView) findViewById(R.id.phaseTwoDisplayTime);
        phaseTwoDisplayScore = (TextView) findViewById(R.id.phaseTwoDisplayScore);
        possibleWords = generateRandomWord(startingLetter);
        int lengthOfArrayList = possibleWords.size();
        int minIndex = 0, maxIndex = lengthOfArrayList - 1;
        Random rex = new Random();
        final int wordKey = rex.nextInt(maxIndex - minIndex + 1) + minIndex;
        String finalWordPerLetter = "";
        finalWordPerLetter = possibleWords.get(wordKey);
        final char[] wordToCharArray = finalWordPerLetter.toCharArray();
        final int[] whichPatternArray = ig.addLettersToHashSet(wordToCharArray);
        System.out.println(whichPatternArray + " seeing which pattern is chosen");
        //phaseTwoDisplayScore.setText(finalWordPerLetter + " word for this grid");


        for(int j = 0; j < cellArrayPhaseTwo.length; j++)
        {
            final Button cellButton = (Button) findViewById(cellArrayPhaseTwo[whichPatternArray[j]]);
            final int jTemp = j;
            char c = (char) wordToCharArray[j];
            final String s = Character.toString(c);
            cellButton.setText(s);
            final SingleCell cell = smallGrid[j];
            System.out.println(jTemp + " checking if for loop is getting touched");


            cellButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String c = cellButton.getText().toString();
                    //pgh.words[jTemp] += c;
                    word += c;
                    System.out.println(word + " seeing if word is getting formed");
                    if(word.length() >= 3)
                    {
                        boolean wordExistsHuh = readFileForWord(word);
                        System.out.println(word + " seeing which word is getting formed");


                        System.out.println(wordExistsHuh + " checking if length of word is >= 3");

                        if(wordExistsHuh)
                        {
                            /*displayWord.setText("Your word " + wordPerGrid);
                            int scoreForThisGrid = ig.pointsPerGrid(wordPerGrid, wordExistsHuh);
                            ig.dynamicScorePerLetter = scoreForThisGrid;
                            ig.perGridScore += ig.dynamicScorePerLetter;
                            phaseTwoDisplayScore.setText("Score: " + ig.perGridScore);
                            if(ig.perGridScore != 0 && ig.perGridScore % 100 == 0)
                                Toast.makeText(getBaseContext(), "Good Job, Keep it going",
                                        Toast.LENGTH_SHORT).show();*/


                            System.out.println("checking if word exists");
                            if(wordsMadeByUser.contains(word))
                            {
                                System.out.println(wordsMadeByUser.contains(word) + " checking if user has made this word");
                                Toast.makeText(getBaseContext(), "You have already made this word",
                                        Toast.LENGTH_SHORT).show();
                            }

                            else
                            {

                                System.out.println("checking if word is added to list");
                                wordsMadeByUser.add(word);
                                ArrayAdapter<String> ad = new ArrayAdapter<String>(ScrogglePhaseTwo.this, R.layout.dummy, wordsMadeByUser);
                                ListView w = (ListView) findViewById(R.id.wordList);
                                w.setAdapter(ad);
                                word = "";

                            }
                        }


                    }




                }
            });









        }
        timer = new CountDownTimer(START, INTERVAL) {
            @Override
            public void onTick(long l) {

                phaseTwoDisplayTime.setText(""+String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(l)),
                        TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(l))));

                if(l <= 10000) {

                    phaseTwoDisplayTime.setTextColor(Color.parseColor("#ff0000"));


                }

                if(l <= 5000)
                {
                    Toast.makeText(getBaseContext(), "Time Remaining:" + TimeUnit.MILLISECONDS.toSeconds(l),
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFinish() {

                Toast.makeText(getBaseContext(), "Game Over, thanks for playing",
                        Toast.LENGTH_SHORT).show();


            }
        }.start();

    }


    public ArrayList<String> generateRandomWord(char startLetterForFunction)
    {
        String wordForLetter = "";
        Resources resource = getResources();
        String finalWordPerLetter = "";
        ArrayList<String> wordsFromLetter = new ArrayList<String>();
        int fileId = resource.getIdentifier(Character.toString(startLetterForFunction), "raw", getPackageName());
        if(fileId != 0)
        {
            InputStream inputStream = getResources().openRawResource(fileId);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if(inputStream != null)
            {
                try{
                    while((wordForLetter = bufferedReader.readLine()) != null)
                    {
                        wordsFromLetter.add(wordForLetter);
                    }
                    /*int lengthOfArrayList = wordsFromLetter.size();
                    int minIndex = 0, maxIndex = lengthOfArrayList - 1;
                    Random rex = new Random();
                    int wordKey = rex.nextInt(maxIndex - minIndex + 1) + minIndex;
                    finalWordPerLetter = wordsFromLetter.get(wordKey);*/
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
        return wordsFromLetter;
    }


    public boolean readFileForWord(String word)
    {
        boolean doesWordExist = false;
        String tempoWord = "";
        Resources resource = getResources();
        String startLetterForFunction = word.substring(0,3);
        int fileId = resource.getIdentifier(startLetterForFunction, "raw", getPackageName());
        if(fileId != 0)
        {
            InputStream inputStream = getResources().openRawResource(fileId);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if(inputStream != null)
            {
                try{
                    while((tempoWord = bufferedReader.readLine()) != null)
                    {
                        if(tempoWord.length() <= 9)
                            wordsFromLetter.add(tempoWord);
                    }

                    for(String s: wordsFromLetter)
                    {
                        if(s.equals(word))
                        {
                            doesWordExist = true;
                        }
                    }
                }

                catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
        return doesWordExist;
    }

}