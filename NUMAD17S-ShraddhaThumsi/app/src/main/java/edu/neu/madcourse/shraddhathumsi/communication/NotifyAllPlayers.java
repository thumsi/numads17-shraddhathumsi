package edu.neu.madcourse.shraddhathumsi.communication;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.shraddhathumsi.R;
public class NotifyAllPlayers extends AppCompatActivity {

    ArrayList<String> listOfAllUsers = new ArrayList<>();
    private DatabaseReference mDatabase;
    ArrayAdapter<String> ad;
    ListView w;
    User user = new User();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_all_players);

        w = (ListView) findViewById(R.id.userList);
        ad = new ArrayAdapter<String>(NotifyAllPlayers.this, R.layout.dummy, listOfAllUsers);
        w.setAdapter(ad);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot childData : dataSnapshot.getChildren()) {
                    String[] sub = childData.toString().split(",");
                    for(String s: sub)
                    {
                        System.out.println(s + " ");
                    }
                    String[] userName = sub[1].split("=");
                    if(userName.length>2)
                        listOfAllUsers.add(userName[2]);
                    ad.notifyDataSetChanged();
                }



                w.setAdapter(ad);
                w.setClickable(true);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        w.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                /*System.out.println(position + " position");
                user.fcm.user.setUserPosInArrayList(position);*/


                if(isNetworkAvailable(view))
                {
                    Toast.makeText(NotifyAllPlayers.this, "You are online!", Toast.LENGTH_SHORT).show();
                }

                else
                {

                    Toast.makeText(NotifyAllPlayers.this, "You are offline but you can still play", Toast.LENGTH_SHORT).show();
                }
                Intent chatboxIntent = new Intent(view.getContext(), CreateNotification.class);
                startActivity(chatboxIntent);
            }
        });
    }

    private boolean isNetworkAvailable(View v) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(v.getContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
