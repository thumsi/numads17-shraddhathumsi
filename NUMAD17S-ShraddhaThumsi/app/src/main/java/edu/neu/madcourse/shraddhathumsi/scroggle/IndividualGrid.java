package edu.neu.madcourse.shraddhathumsi.scroggle;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

/**
 * Created by shraddha on 2/21/17.
 */

public class IndividualGrid {
    boolean isValidMove, isValidWord;
    int individualGridRowNo, individualGridColNo;
    Stack<Integer> previouslyClickedStack = new Stack<>();
    Queue<Character> lettersOfWordInAQueue = new LinkedList<Character>();
    SingleCell[][] cellOutLine;
    boolean clickableLetter;
    int scorePerGrid = 0;
    ScroggleGameStarter sgs;
    int[][] patterns = {{0,1,2,5,8,7,6,3,4}, {6,3,0,1,4,7,8,5,2}, {2,1,0,3,4,5,8,7,6}, {0,3,6,7,8,5,4,1,2}, {4,0,3,6,7,8,5,2,1}, {8,7,6,4,5,2,1,0,3}, {2,1,0,3,4,5,8,7,6}, {4,8,5,2,1,0,3,6,7}, {6,3,0,1,4,7,8,5,2}};
    static ArrayList<Character> wordLetterHashSet = new ArrayList<>();
    HashMap<Integer, ArrayList<Integer>> mapPossibleMoves = new HashMap<>();
    ArrayList<Integer> movesForZero, movesForOne, movesForTwo, movesForThree, movesForFour, movesForFive, movesForSix, movesForSeven, movesForEight;
    ScrogglePhaseTwo spt;


    String dynamicWordByTheLetterPerGrid;



    IndividualGrid(ScroggleGameStarter scr)
    {
        this.sgs = scr;
    }
    IndividualGrid(ScrogglePhaseTwo spt){
        this.spt = spt;
    }


    boolean isLetterClickable(int charIndex)
    {
        System.out.println(previouslyClickedStack + " see what stack is there before pop");
        clickableLetter = true;
        if(previouslyClickedStack.isEmpty())
        {
            previouslyClickedStack.push(charIndex);
            clickableLetter = true;
        }

        else {

            int alreadyPresentButton = previouslyClickedStack.peek();
            System.out.println(alreadyPresentButton + " already present");
            System.out.println(charIndex + " currently clicked");
            if (alreadyPresentButton == 0) {
                movesForZero = new ArrayList<>();
                movesForZero.add(1);
                movesForZero.add(3);
                movesForZero.add(4);
                mapPossibleMoves.put(0,movesForZero);

            } else if (alreadyPresentButton == 1) {
                movesForOne = new ArrayList<>();
                movesForOne.add(0);
                movesForOne.add(3);
                movesForOne.add(4);
                movesForOne.add(5);
                movesForOne.add(2);
                mapPossibleMoves.put(1,movesForOne);


            } else if (alreadyPresentButton == 2) {
                movesForTwo = new ArrayList<>();
                movesForTwo.add(1);
                movesForTwo.add(4);
                movesForTwo.add(5);
                mapPossibleMoves.put(2,movesForTwo);
            } else if (alreadyPresentButton == 3) {
                movesForThree = new ArrayList<>();
                movesForThree.add(0);
                movesForThree.add(1);
                movesForThree.add(4);
                movesForThree.add(7);
                movesForThree.add(6);
                mapPossibleMoves.put(3,movesForThree);

            } else if (alreadyPresentButton == 4) {
                movesForFour = new ArrayList<>();
                movesForFour.add(0);
                movesForFour.add(1);
                movesForFour.add(2);
                movesForFour.add(3);
                movesForFour.add(5);
                movesForFour.add(7);
                movesForFour.add(6);
                movesForFour.add(8);
                mapPossibleMoves.put(4,movesForFour);
                //clickableLetter = true;
            }
            else if(alreadyPresentButton == 5)
            {
                movesForFive = new ArrayList<>();
                movesForFive.add(2);
                movesForFive.add(1);
                movesForFive.add(4);
                movesForFive.add(7);
                movesForFive.add(8);
                mapPossibleMoves.put(5,movesForFive);
            }

            else if(alreadyPresentButton == 6)
            {
                movesForSix = new ArrayList<>();
                movesForSix.add(3);
                movesForSix.add(4);
                movesForSix.add(7);
                mapPossibleMoves.put(6,movesForSix);
            }

            else if(alreadyPresentButton == 7)
            {
                movesForSeven = new ArrayList<>();
                movesForSeven.add(6);
                movesForSeven.add(3);
                movesForSeven.add(4);
                movesForSeven.add(5);
                movesForSeven.add(8);
                mapPossibleMoves.put(7,movesForSeven);
            }

            else if(alreadyPresentButton == 8)
            {
                movesForEight = new ArrayList<>();
                movesForEight.add(7);
                movesForEight.add(4);
                movesForEight.add(5);
                mapPossibleMoves.put(8,movesForEight);
            }


            //ArrayList<Integer> movesForGivenIndex = new ArrayList<>();
            ArrayList<Integer> movesForGivenIndex = mapPossibleMoves.get(alreadyPresentButton);
            for(int i: movesForGivenIndex)
            {
                if(i == charIndex)
                {
                    System.out.println(i + " possible moves for " + charIndex);
                    previouslyClickedStack.push(charIndex);
                    clickableLetter = true;
                }
                else clickableLetter = false;
            }


        }

        return clickableLetter;

    }

    int pointsPerGrid(String word, boolean isWordAValidOne)
    {
        if(isWordAValidOne)
        {
            scorePerGrid = calculateScorePerLetter(word) + calculateBonusPointsPerGrid(word);
        }
        return scorePerGrid;
    }

    int calculateScorePerLetter(String inputWord)
    {
        int scorePerLetter = 0;
        char[] wordToCharArray = inputWord.toCharArray();
        for(char c : wordToCharArray)
        {
            String charac = Character.toString(c).toUpperCase();
            switch (charac)
            {
                case "A":case "E":case "I":case "L":case "N":case "O":case "R":case "S":case "T":case "U":
                scorePerLetter = 1;
                break;

                case "D":case "G": scorePerLetter = 2; break;

                case "B":case "C":case "M":case "P": scorePerLetter = 3; break;

                case "F":case "H":case "V":case "W":case "Y": scorePerLetter = 4; break;

                case "K": scorePerLetter = 5; break;

                case "J":case "X": scorePerLetter = 8; break;

                case "Q":case "Z": scorePerLetter = 10; break;

                default: scorePerLetter = 0;
            }
        }
        return scorePerLetter;
    }

    int calculateBonusPointsPerGrid(String inputWord)
    {
        int bonusPointsPerGrid = 0;
        int lengthOfInputWord = inputWord.length();
        switch (lengthOfInputWord)
        {
            case 3:case 4: bonusPointsPerGrid = lengthOfInputWord + 2; break;
            case 5:case 6: bonusPointsPerGrid = lengthOfInputWord + 3; break;
            case 7:case 8: bonusPointsPerGrid = lengthOfInputWord + 7; break;
            case 9: bonusPointsPerGrid = lengthOfInputWord + 25; break;
            default: bonusPointsPerGrid = 0;
        }

        return bonusPointsPerGrid;
    }


    int[] addLettersToHashSet(char[] ltow)
    {
        int min = 0, max = 8;
        Random r = new Random();
        int whichPattern = r.nextInt(max - min + 1) + min;
        int[] whichPatternArray = new int[9];
        whichPatternArray = patterns[whichPattern];


        for(int i = 0; i < 9; i++)
        {
            wordLetterHashSet.add(ltow[i]);

        }
        System.out.println(wordLetterHashSet + "examinng which hashset has been formed");
        System.out.println((int[]) whichPatternArray + " examinining what pattern has been picked");
        return whichPatternArray;
    }
}
