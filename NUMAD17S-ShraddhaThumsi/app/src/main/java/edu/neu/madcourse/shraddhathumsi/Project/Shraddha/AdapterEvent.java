package edu.neu.madcourse.shraddhathumsi.Project.Shraddha;

/**
 * Created by manusaxena on 3/4/17.
 *
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import edu.neu.madcourse.shraddhathumsi.Project.Shraddha.model.SPEvent;
import edu.neu.madcourse.shraddhathumsi.R;


public class AdapterEvent extends ArrayAdapter<SPEvent> {


    private ArrayList<SPEvent> lEvent;

    public String getUser() {
        return event;
    }

    public void setUser(String event) {
        this.event = event;
    }

    private String event;
    private static LayoutInflater inflater = null;
    private static DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference();
    private static String clientToken= FirebaseInstanceId.getInstance().getToken();





    public AdapterEvent(Activity activity, int textViewResourceId, ArrayList<SPEvent> _lEvent) {
        super(activity, textViewResourceId, _lEvent);
        try {
            this.lEvent = _lEvent;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {

        }
    }


    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public Button addSoundProfile;
        public Button editSoundProfile;
        public TextView title;
        public TextView location;
        public TextView time;
        public TextView currentSound;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.simple_list_item_2, null);
                holder = new ViewHolder();

                holder.addSoundProfile = (Button) vi.findViewById(R.id.addSoundProfile);
                holder.editSoundProfile = (Button) vi.findViewById(R.id.editSoundProfile);
                holder.title = (TextView) vi.findViewById(R.id.sptitle);
                holder.location = (TextView) vi.findViewById(R.id.splocation);
                holder.time = (TextView) vi.findViewById(R.id.sptime);
                holder.currentSound = (TextView) vi.findViewById(R.id.currentSound);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }



            holder.title.setText(lEvent.get(position).getTitle());
            holder.location.setText("@"+lEvent.get(position).getLocation());
            holder.time.setText("From: "+lEvent.get(position).getStartTime()+"-"+lEvent.get(position).getEndTime());

            if(lEvent.get(position).getCurrentSoundProfile().equalsIgnoreCase("Default"))
            {
                holder.currentSound.setVisibility(View.GONE);
                holder.editSoundProfile.setVisibility(View.GONE);
                holder.addSoundProfile.setVisibility(View.VISIBLE);
                holder.currentSound.setVisibility(View.GONE);
            }
            else
            {   holder.addSoundProfile.setVisibility(View.GONE);
                holder.editSoundProfile.setVisibility(View.VISIBLE);
                holder.currentSound.setVisibility(View.VISIBLE);
                holder.currentSound.setText(lEvent.get(position).getCurrentSoundProfile());
            }

            holder.addSoundProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//
                    final Intent intent = new Intent(getContext(), AddSoundProfile.class);
                    intent.putExtra(AddSoundProfile.KEY_RESTORE, lEvent.get(position).getKey());
                    getContext().startActivity(intent);
                }
            });


            holder.editSoundProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//
                    final Intent intent = new Intent(getContext(), EditSoundProfile.class);
                    intent.putExtra(EditSoundProfile.KEY_RESTORE, lEvent.get(position).getKey());
                    getContext().startActivity(intent);
                }
            });

        } catch (Exception e) {


        }
        return vi;
    }
}
