package edu.neu.madcourse.shraddhathumsi;
//test git push from terminal
//about to add communication
import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.content.Context;
import android.telephony.TelephonyManager;

public class DisplayAboutActivity extends AppCompatActivity {

    private int REQUEST_IMEI =123;
    TextView textDeviceID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shraddha_display_about_);
        getSupportActionBar().setTitle("Shraddha Thumsi");
         textDeviceID = (TextView)findViewById(R.id.uniqueDeviceId);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // IMEI permission has not been granted.

            requestIMEIPermission();

        } else {

            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            textDeviceID.setText(getDeviceID(telephonyManager));
        }


        // code credits for IMEID: Stack overflow

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            int[] grantResults) {

        if (requestCode == REQUEST_IMEI) {

            // Received permission result for camera permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // call permission has been granted, preview can be displayed

                TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                textDeviceID.setText(getDeviceID(telephonyManager));
            } else {

                textDeviceID.setText("IMEI not available");
            }
        }
    }

    private void requestIMEIPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_PHONE_STATE},REQUEST_IMEI);


        } else {

            // call permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_IMEI);
        }
    }







    String getDeviceID(TelephonyManager phonyManager){

        String id = phonyManager.getDeviceId();
        if (id == null){
            id = "not available";
        }

        int phoneType = phonyManager.getPhoneType();
        switch(phoneType){
            case TelephonyManager.PHONE_TYPE_NONE:
                return "NONE: " + id;

            case TelephonyManager.PHONE_TYPE_GSM:
                return "GSM: IMEI=" + id;

            case TelephonyManager.PHONE_TYPE_CDMA:
                return "CDMA: MEID/ESN=" + id;

            case TelephonyManager.PHONE_TYPE_SIP:
                return "SIP";

 /*
  *  for API Level 11 or above
  *  case TelephonyManager.PHONE_TYPE_SIP:
  *   return "SIP";
  */

            default:
                return "UNKNOWN: ID=" + id;
        }

    }


    public void loadMainMenu(View view)
    {

        Intent intent = new Intent(this, AssignmentOneMainActivity.class);
        startActivity(intent);
    }
}
