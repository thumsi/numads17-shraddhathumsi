package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import edu.neu.madcourse.shraddhathumsi.R;
import edu.neu.madcourse.shraddhathumsi.communication.CreateNotification;
import edu.neu.madcourse.shraddhathumsi.communication.NotifyAllPlayers;
import java.util.Calendar;
import android.content.Intent;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.ArrayList;
import android.net.ConnectivityManager;
import 	android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Collections;
import android.text.format.Time;
import 	java.text.SimpleDateFormat;
public class ViewAllPlayersAssign extends AppCompatActivity {
    ArrayList<String> listOfAllUsers = new ArrayList<>();
    private DatabaseReference mDatabase;
    ArrayAdapter<String> ad;
    ListView w;
    UserAssign user1, user2, stateHolder = new UserAssign();
    UserAssign currentlyLoggedInUser, opponent;
    GameStarterAssign gameStarterAssign;
    TextView topScore;
    Long timeStamp;
    String timeStampValue = "", topScoreValue;
    ArrayList<DataSnapshot> allUsers = new ArrayList<>();
    ArrayList<String> allScores = new ArrayList<>();

    public void setGameStarterAssign(GameStarterAssign gameStarterAssign1)
    {
        this.gameStarterAssign = gameStarterAssign1;
    }
    public GameStarterAssign getGameStarterAssign()
    {
        return this.gameStarterAssign;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater factory = LayoutInflater.from(this);
        final View rootView = factory.inflate(R.layout.activity_view_all_players_assign, null);
        setContentView(rootView);
        w = (ListView) findViewById(R.id.userListAssign);
        topScore = (TextView) findViewById(R.id.topScorer);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Intent intent = getIntent();
        final String loggedInKey = intent.getStringExtra("keyOfLoggedIn");
        ad = new ArrayAdapter<String>(ViewAllPlayersAssign.this, R.layout.dummy, listOfAllUsers);

        mDatabase.child("users")
                .orderByChild("score")
                .addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot childData, String s) {

                allUsers.add(childData);
                //sortForBestScore(allUsers);
                System.out.println(allUsers.size() + " on child data");
                stateHolder.setDataSnapshotArrayList(allUsers);
                String[] sub = childData.toString().split(",");
                String defaultCountry = "US";
                String[] userName = sub[1].split("=");
                String[] countryName = sub[2].split("=");
                if(userName.length>2)
                {
                    if(!listOfAllUsers.contains(userName[2]))
                    {

                        if(countryName[1].equals(String.valueOf(0)))
                        {
                            listOfAllUsers.add(userName[2]+" Country:" +defaultCountry);
                        }

                        else listOfAllUsers.add(userName[2]+" Country:" +countryName[1]);
                    }
                }
                ad.notifyDataSetChanged();
                /*for (DataSnapshot childData : dataSnapshot.getChildren()) {
                    //System.out.println((String.valueOf(childData.getValue(UserAssign.class).getEmail())) + " child data to see what i get");
                    allUsers.add(childData);
                    sortForBestScore(allUsers);
                    System.out.println(allUsers.size() + " on child data");
                    stateHolder.setDataSnapshotArrayList(allUsers);
                    String[] sub = childData.toString().split(",");
                    String defaultCountry = "US";
                    String[] userName = sub[1].split("=");
                    String[] countryName = sub[2].split("=");
                    if(userName.length>2)
                    {
                        if(!listOfAllUsers.contains(userName[2]))
                        {

                            if(countryName[1].equals(String.valueOf(0)))
                            {
                                listOfAllUsers.add(userName[2]+" Country:" +defaultCountry);
                            }

                            else listOfAllUsers.add(userName[2]+" Country:" +countryName[1]);
                        }
                    }
                    ad.notifyDataSetChanged();
                }*/
                w.setAdapter(ad);
                w.setClickable(true);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                for (DataSnapshot childData : dataSnapshot.getChildren()) {
                    //System.out.println((String.valueOf(childData.getValue(UserAssign.class).getEmail())) + " child data to see what i get");
                    allUsers.add(childData);
                    //sortForBestScore(allUsers);
                    System.out.println(allUsers.size() + " on child data");
                    stateHolder.setDataSnapshotArrayList(allUsers);
                    String[] sub = childData.toString().split(",");
                    String defaultCountry = "US";
                    String[] userName = sub[1].split("=");
                    if(userName.length>2)
                    {
                        if(!listOfAllUsers.contains(userName[2]))
                        {
                            listOfAllUsers.add(userName[2]);
                        }
                    }

                    /*String[] countryName = sub[2].split("=");
                    if(userName.length>2)
                    {
                        if(!listOfAllUsers.contains(userName[2]))
                        {

                            if(countryName[1].equals(String.valueOf(0)))
                            {
                                listOfAllUsers.add(userName[2]+" Country:" +defaultCountry);
                            }

                            else listOfAllUsers.add(userName[2]+" Country:" +countryName[1]);
                        }
                    }*/
                    ad.notifyDataSetChanged();
                }
                w.setAdapter(ad);
                w.setClickable(true);


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });









        /*mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childData : dataSnapshot.getChildren()) {
                    System.out.println(childData.getValue(UserAssign.class) + " child data to see what i get");
                    allUsers.add(childData);
                    sortForBestScore(allUsers);
                    System.out.println(allUsers.size() + " on child data");
                    stateHolder.setDataSnapshotArrayList(allUsers);
                    String[] sub = childData.toString().split(",");
                    String defaultCountry = "US";
                    for(String s: sub)
                    {
                        System.out.println(s + " ");
                    }
                    String[] userName = sub[1].split("=");
                    String[] countryName = sub[2].split("=");
                    if(userName.length>2)
                    {
                        if(!listOfAllUsers.contains(userName[2]))
                        {

                            if(countryName[1].equals(String.valueOf(0)))
                            {
                                listOfAllUsers.add(userName[2]+" Country:" +defaultCountry);
                            }

                            else listOfAllUsers.add(userName[2]+" Country:" +countryName[1]);
                        }
                    }
                    ad.notifyDataSetChanged();
                }
                w.setAdapter(ad);
                w.setClickable(true);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

        w.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                System.out.println(position+ " position received at item listener");
                DataSnapshot dat = allUsers.get(position);
                UserAssign userAssign = dat.getValue(UserAssign.class);
                opponent = userAssign;
                System.out.println(userAssign.getEmail() + " user email at position " + position);
                String token = FirebaseInstanceId.getInstance().getToken();
                opponent.setToken(token);
                opponent.setKey(dat.getKey());
                String key = opponent.getKey();
                String email = opponent.getEmail();
                String userName = opponent.getUsername();
                String score = opponent.getScore();
                Intent chatboxIntent = new Intent(rootView.getContext(), CreateNotificationAssign.class);
                chatboxIntent.putExtra("clienttoken",opponent.getToken());
                chatboxIntent.putExtra("email", email);
                chatboxIntent.putExtra("key", key);
                chatboxIntent.putExtra("userName", userName);
                chatboxIntent.putExtra("score", score);
                chatboxIntent.putExtra("loggedInKey", loggedInKey);
                startActivity(chatboxIntent);
            }
        });

    }


    public void sortForBestScore(ArrayList<DataSnapshot> allUsers){

        System.out.println(allUsers.get(0).getValue() + " from sort ");
        UserAssign temp = allUsers.get(0).getValue(UserAssign.class);
        ArrayList<DataSnapshot> tempoArray = stateHolder.getDataSnapshotArrayList();
        System.out.println(allUsers.size() + " all users size before bubble sort after using setter getter");
        int n = allUsers.size();
        /*int n = allUsers.size();

            for(int i = 0; i < n; i++)
            {
                for(int j = 1; j < n - i; j++)
                {
                    user1 = allUsers.get(j-1).getValue(UserAssign.class);
                    user2 = allUsers.get(j).getValue(UserAssign.class);
                    if(user1.getScore().compareToIgnoreCase(user2.getScore()) > 0)
                    {
                        temp = user1;
                        user1 = user2;
                        user2 = temp;
                    }
                }
            }*/
            //temp = allUsers.get(n-1).getValue(UserAssign.class);



        //if(user1.getScore().compareToIgnoreCase(user2.getScore()) > 0)

        UserAssign greatest = temp;
        timeStamp = System.currentTimeMillis();
        Calendar c = Calendar.getInstance();
        Time now = new Time();
        now.setToNow();
        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = df.format(c.getTime());
        if(allUsers.isEmpty())
        {
            timeStampValue = String.valueOf("Best Score: " + "\n" + "Email: " + greatest.getEmail() + " Score: " + greatest.getScore() + " " + formattedDate + "--" + now.hour + ":" + now.minute);
            topScore.setText(timeStampValue);
        }
        else
        {
            //greatest = allUsers.get(allUsers.size() - 1).getValue(UserAssign.class);
            timeStampValue = String.valueOf("Best Score: " + "\n" + "Email: " + allUsers.get(n-1).getValue(UserAssign.class).getEmail() + " Score: " + allUsers.get(n-1).getValue(UserAssign.class).getScore() + " " + formattedDate + "--" + now.hour + ":" + now.minute);
            topScore.setText(timeStampValue);
        }


    }

    public UserAssign getPlayerB()
    {
        return this.opponent;
    }
}