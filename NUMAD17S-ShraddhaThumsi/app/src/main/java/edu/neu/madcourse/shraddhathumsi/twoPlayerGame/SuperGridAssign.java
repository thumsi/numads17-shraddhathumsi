package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;

import java.util.HashMap;
import java.util.Random;

import edu.neu.madcourse.shraddhathumsi.scroggle.ScroggleGameStarter;
import edu.neu.madcourse.shraddhathumsi.scroggle.ScrogglePhaseTwo;


/**
 * Created by shraddha on 2/20/17.
 */

public class SuperGridAssign {

    int individualGridRowNo, individualGridColNo;
    static HashMap<Integer, Character> aToZ = new HashMap<Integer,Character>();
    GameStarterAssign sgs1;
    OnePlayerGameStarter onePlayerGameStarter;
    ScrogglePhaseTwo spt;
    int GRID_SIZE = 9;
    String[] wordPerGrid = new String[GRID_SIZE];
    int[] scorePerGrid = new int[GRID_SIZE];
    int fullScore = 0;
    SuperGridAssign(GameStarterAssign sgs)
    {
        this.sgs1 = sgs;
        for(int i = 0; i < GRID_SIZE; i++)
        {
            wordPerGrid[i] = "";
            scorePerGrid[i] = 0;
        }
    }
    SuperGridAssign(OnePlayerGameStarter onePlayerGameStarter)
    {
        this.onePlayerGameStarter = onePlayerGameStarter;
        for(int i = 0; i < GRID_SIZE; i++)
        {
            wordPerGrid[i] = "";
            scorePerGrid[i] = 0;
        }
    }
    static HashMap<Integer,Character> setHashMap()
    {
        char letter;
        for(int i = 0; i < 26; i++)
        {
            letter = (char) (i + 97);
            aToZ.put(i,letter);
        }
        return aToZ;
    }

    char selectStartingLetter(HashMap<Integer, Character> map)
    {
        Random r = new Random();
        int min = 0, max = 25;
        int letterKey = r.nextInt(max - min + 1) + min;
        char c = (char) (letterKey + 97);
        map.put(letterKey, c);
        char top = map.get(letterKey);
        return top;
    }




}
