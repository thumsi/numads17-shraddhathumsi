package edu.neu.madcourse.shraddhathumsi.scroggle;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.os.CountDownTimer;
import java.util.HashSet;
import java.util.Random;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import edu.neu.madcourse.shraddhathumsi.R;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import android.graphics.Color;
import android.widget.Toast;
import android.widget.Button;

import java.util.concurrent.TimeUnit;
import java.util.Set;
import android.view.LayoutInflater;
public class ScroggleGameStarter extends AppCompatActivity {

    int[] cellArray = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};
    private int gridArray[] = {R.id.large1, R.id.large2, R.id.large3,
            R.id.large4, R.id.large5, R.id.large6, R.id.large7, R.id.large8,
            R.id.large9,};

    private Set<SingleCell> availableMoves = new HashSet<SingleCell>();
    public HashMap<Integer, Character> superGridMap;
    public char startLetter;
    TextView displayWord, displayScore, displayTime;
    boolean isThisLetterClickable;
    private final int START = 90000, INTERVAL = 1000;
    CountDownTimer cdt;
    String wordPerGrid = new String();
    SuperGrid spg = new SuperGrid(this);
    IndividualGrid ig = new IndividualGrid(this);
    SingleCell sc = new SingleCell(this);
    ScrogglePhaseTwo spt = new ScrogglePhaseTwo();
    PhaseTwoHelperGrid ptg = new PhaseTwoHelperGrid(this);
    int GRID_SIZE = cellArray.length;
    SingleCell[] bigGrid = new SingleCell[GRID_SIZE];
    SingleCell[][] smallGrid = new SingleCell[GRID_SIZE][GRID_SIZE];





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater factory = LayoutInflater.from(this);
        final View rootView = factory.inflate(R.layout.activity_scroggle_game_starter, null);
        setContentView(rootView);
        sc.setFullView(rootView);

        superGridMap = spg.setHashMap();

        displayWord = (TextView) findViewById(R.id.displayWord);
        displayScore = (TextView) findViewById(R.id.displayScore);
        displayTime = (TextView) findViewById(R.id.displayTime);

        for(int i = 0; i < gridArray.length; i++)
        {
            final View outer = rootView.findViewById(gridArray[i]);
            sc.setFullView(outer);
            startLetter = spg.selectStartingLetter(superGridMap);
            System.out.println(startLetter + " checking which character is received");
            ArrayList<String> wordsFromLetter = generateRandomWord(startLetter);
            int lengthOfArrayList = wordsFromLetter.size();
            int minIndex = 0, maxIndex = lengthOfArrayList - 1;
            Random rex = new Random();
            int wordKey = rex.nextInt(maxIndex - minIndex + 1) + minIndex;
            String finalWordPerLetter = "";
            finalWordPerLetter = wordsFromLetter.get(wordKey);
            final char[] wordToCharArray = finalWordPerLetter.toCharArray();
            //displayWord.setText("LatestWord: " + finalWordPerLetter);



            final int[] whichPatternArray = ig.addLettersToHashSet(wordToCharArray);
            for(int j = 0; j < cellArray.length; j++)
            {
                //final Button cellButton = (Button) outer.findViewById(cellArray[whichPatternArray[j]]); correct
                final Button cellButton = (Button) outer.findViewById(cellArray[whichPatternArray[j]]);
                final SingleCell cell = smallGrid[i][j];
                final int jTemp = j, iTemp = i;
                char c = (char) wordToCharArray[j];
                final String s = Character.toString(c);
                cellButton.setText(s);

                cellButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        String c = cellButton.getText().toString();

                        //isThisLetterClickable = ig.isLetterClickable(jTemp);
                        isThisLetterClickable = ig.isLetterClickable(whichPatternArray[jTemp]);


                        /*if(isThisLetterClickable)
                        {*/
                        //wordPerGrid += c;

                        spg.wordPerGrid[iTemp] += c;
                        cellButton.setBackgroundColor(Color.parseColor("#ff0000"));
                        if(spg.wordPerGrid[iTemp].length() >= 3)
                        {
                            boolean wordExistsHuh = readFileForWord(spg.wordPerGrid[iTemp]);
                            if(wordExistsHuh)
                            {
                                displayWord.setText("Your word: " + spg.wordPerGrid[iTemp]);
                                int scoreForThisGrid = ig.pointsPerGrid(spg.wordPerGrid[iTemp], wordExistsHuh);
                                //ig.dynamicScorePerLetter = scoreForThisGrid;
                                //ig.perGridScore += ig.dynamicScorePerLetter;
                                spg.scorePerGrid[iTemp] = scoreForThisGrid;
                                spg.fullScore += spg.scorePerGrid[iTemp];
                                displayScore.setText("Score: " + spg.fullScore);
                                if(spg.scorePerGrid[iTemp] != 0 && spg.scorePerGrid[iTemp] % 100 == 0)
                                    Toast.makeText(getBaseContext(), "Good Job, Keep it going",
                                            Toast.LENGTH_SHORT).show();



                            }
                        }


                       /* }

                        else
                        {

                        }*/

                        System.out.println(wordPerGrid + " check if button is getting clicked");
                    }
                });

            }




        }


        cdt = new CountDownTimer(START, INTERVAL) {
            @Override
            public void onTick(long l) {

                displayTime.setText(""+String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(l)),
                        TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(l))));

                if(l <= 10000) {

                    displayTime.setTextColor(Color.parseColor("#ff0000"));


                }

                if(l <= 5000)
                {
                    Toast.makeText(getBaseContext(), "Time Remaining:" + TimeUnit.MILLISECONDS.toSeconds(l),
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(rootView.getContext(), ScrogglePhaseTwo.class);

                startActivity(intent);


            }
        }.start();



    }





    // file reading needs to be done in this activity since it involves android supported resource file reading

    public ArrayList<String> generateRandomWord(char startLetterForFunction)
    {
        String wordForLetter = "";
        Resources resource = getResources();
        String finalWordPerLetter = "";
        ArrayList<String> wordsFromLetter = new ArrayList<String>();
        int fileId = resource.getIdentifier(Character.toString(startLetterForFunction), "raw", getPackageName());
        if(fileId != 0)
        {
            InputStream inputStream = getResources().openRawResource(fileId);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if(inputStream != null)
            {
                try{
                    while((wordForLetter = bufferedReader.readLine()) != null)
                    {
                        wordsFromLetter.add(wordForLetter);
                    }
                    /*int lengthOfArrayList = wordsFromLetter.size();
                    int minIndex = 0, maxIndex = lengthOfArrayList - 1;
                    Random rex = new Random();
                    int wordKey = rex.nextInt(maxIndex - minIndex + 1) + minIndex;
                    finalWordPerLetter = wordsFromLetter.get(wordKey);*/
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
        return wordsFromLetter;
    }


    public boolean readFileForWord(String word)
    {
        System.out.println(word + " checking if word per grid is sent");
        boolean doesWordExist = false;
        String tempoWord = "";
        Resources resource = getResources();
        ArrayList<String> subSetWith9LetteredWords = new ArrayList<>();
        ArrayList<String> wordsFromLetter = new ArrayList<String>();
        String startLetterForFunction = word.substring(0,3);
        int fileId = resource.getIdentifier(startLetterForFunction, "raw", getPackageName());
        if(fileId != 0)
        {
            InputStream inputStream = getResources().openRawResource(fileId);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if(inputStream != null)
            {
                try{
                    while((tempoWord = bufferedReader.readLine()) != null)
                    {
                        if(tempoWord.length() <= 9)
                            wordsFromLetter.add(tempoWord);
                    }

                    for(String s: wordsFromLetter)
                    {
                        if(s.equals(word))
                        {
                            doesWordExist = true;
                        }
                    }
                }

                catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
        return doesWordExist;
    }

}
