package edu.neu.madcourse.shraddhathumsi.communication;

/**
 * Created by shraddha on 3/1/17.
 */

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

/**
 * Created by aniru on 2/18/2017.
 */
//test git configs
@IgnoreExtraProperties
public class User {

    public String username;
    public String score;
    public String token, email;
    public int userPosInArrayList;
    public Communication fcm;
    ArrayList<String> listOfAllUsers = new ArrayList<>();
    ArrayList<DataSnapshot> dataSnapshotArrayList = new ArrayList<>();


    public User(){
        // Default constructor required for calls to DataSnapshot.getValue(UserAssign.class)
    }
    public User(String username, String score, String email, String token)
    {
        this.username = username;
        this.score = score;
        this.token = token;
        this.email = email;
    }

    public User(Communication fcm)
    {
        this.fcm = fcm;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setScore(String score)
    {
        this.score = score;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }


    public String getUsername()
    {
        return this.username;
    }

    public String getScore()
    {
        return this.score;
    }

    public String getToken()
    {
        return this.token;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setUserPosInArrayList(int p)
    {
        System.out.println(p + " position received at user class");
        this.userPosInArrayList = p;
    }

    public int getUserPosInArrayList()
    {

        return this.userPosInArrayList;
    }

    public void setListOfAllUsers(ArrayList<String> listOfAllUsers)
    {
        this.listOfAllUsers = listOfAllUsers;
    }

    public ArrayList<String> getListOfAllUsers()
    {
        return this.listOfAllUsers;
    }

    public void setDataSnapshotArrayList(ArrayList<DataSnapshot> dps)
    {
        this.dataSnapshotArrayList = dps;
    }

    public ArrayList<DataSnapshot> getDataSnapshotArrayList()
    {
        return this.dataSnapshotArrayList;
    }



}