package edu.neu.madcourse.shraddhathumsi.scroggle;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import edu.neu.madcourse.shraddhathumsi.R;


public class WordGame extends AppCompatActivity {

    Button newGame, continueGame, creditsButton, infoButton;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroggle_single_player_word_game);
        newGame = (Button) findViewById(R.id.newGame);
        continueGame = (Button) findViewById(R.id.continueGame);
        creditsButton = (Button) findViewById(R.id.creditsButton);
        infoButton = (Button) findViewById(R.id.infoButton);
        newGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ScroggleGameStarter.class);
                startActivity(intent);
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), InfoScroggle.class);
                startActivity(intent);
            }
        });

        creditsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), AcknowledgementsScroggle.class);
                startActivity(intent);
            }
        });
    }


}


//credits
// layout inflation without fragments: http://bit.ly/2lKKxq3
// empty constructor for activity: http://bit.ly/2llAc0F
// ascii values: http://bit.ly/1l3DiBZ
// reading file by name: http://bit.ly/2mi94jh