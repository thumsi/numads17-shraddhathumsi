package edu.neu.madcourse.shraddhathumsi.scroggle;

/**
 * Created by shraddha on 2/21/17.
 */
import android.view.View;
public class SingleCell {
    boolean isCellSelected;
    String textOnCell;
    String cellBgColor;
    int individualCellNo;
    ScroggleGameStarter sgs;
    View fullView;
    SingleCell[] sca;
    SingleCell(ScroggleGameStarter s)
    {
        this.sgs = s;
    }

    void setSelected(boolean selectedValue)
    {
        this.isCellSelected = selectedValue;
    }

    boolean getSelected()
    {
        return this.isCellSelected;
    }

    void setTextOnCell(char t)
    {
        String tex = Character.toString(t);
        this.textOnCell = tex;
    }

    String getTextOnCell()
    {
        return this.textOnCell;
    }

    void setBgColor(String color)
    {
        this.cellBgColor = color;
    }

    String getBgColor()
    {
        return this.cellBgColor;
    }

    void setFullView(View view)
    {
        this.fullView = view;
    }

    View getFullView()
    {
        return this.fullView;
    }

    void setSubTiles(SingleCell[] cellArray)
    {
        this.sca = cellArray;
    }

    SingleCell[] getSubTiles()
    {
        return this.sca;
    }


}
