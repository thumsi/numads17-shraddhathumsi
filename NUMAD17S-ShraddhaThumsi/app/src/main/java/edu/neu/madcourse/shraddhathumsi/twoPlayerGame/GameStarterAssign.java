package edu.neu.madcourse.shraddhathumsi.twoPlayerGame;
//testing github vs bitbucket
import android.os.Vibrator;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import edu.neu.madcourse.shraddhathumsi.R;
import edu.neu.madcourse.shraddhathumsi.scroggle.IndividualGrid;
import edu.neu.madcourse.shraddhathumsi.scroggle.PhaseTwoHelperGrid;
import edu.neu.madcourse.shraddhathumsi.scroggle.ScrogglePhaseTwo;
import edu.neu.madcourse.shraddhathumsi.scroggle.SingleCell;
import edu.neu.madcourse.shraddhathumsi.scroggle.SuperGrid;
import edu.neu.madcourse.shraddhathumsi.tictactoe.GameActivity;
import edu.neu.madcourse.shraddhathumsi.tictactoe.Tile;
import  android.media.ToneGenerator;
import android.media.AudioManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import android.graphics.Color;
import android.widget.Toast;
import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.os.CountDownTimer;

import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Random;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.Set;
import android.content.Context;
import android.view.LayoutInflater;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class GameStarterAssign extends AppCompatActivity {

    int[] cellArray = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};
    private int gridArray[] = {R.id.large1, R.id.large2, R.id.large3,
            R.id.large4, R.id.large5, R.id.large6, R.id.large7, R.id.large8,
            R.id.large9,};
    public HashMap<Integer, Character> superGridMap;
    public char startLetter;
    Button quitGame;
    TextView displayWord, displayScore, displayTime;
    boolean isThisLetterClickable;
    private final int START = 90000, INTERVAL = 1000;
    CountDownTimer cdt;
    final int MAX_WAITING_TIME = 10000;
    ProgressBar progressBar;
    String clientToken, key, userName, email, score;
    String wordPerGrid = new String();
    SuperGridAssign spg = new SuperGridAssign(this);
    IndividualGridAssign ig = new IndividualGridAssign(this);
    TwoPlayerAssign twoPac = new TwoPlayerAssign();
    UserAssign currentlyLoggedInUser = twoPac.currentlyLoggedInUser;
    private IndividualGridAssign.OWNER_OF_TURN currentPlayer = IndividualGridAssign.OWNER_OF_TURN.PLAYER_A;
    UserAssign playerA = new UserAssign(this);
    UserAssign playerB;
    ViewAllPlayersAssign viewAllPlayersAssign = new ViewAllPlayersAssign();
    SingleCellAssign sc = new SingleCellAssign(this);
    int GRID_SIZE = cellArray.length;
    private DatabaseReference mDatabase;
    GameHelperClass gameHelperClass = new GameHelperClass(this);
    SingleCell[] bigGrid = new SingleCell[GRID_SIZE];
    SingleCell[][] smallGrid = new SingleCell[GRID_SIZE][GRID_SIZE];
    private Handler mHandler = new Handler();
    static String loggedInKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater factory = LayoutInflater.from(this);
        final View rootView = factory.inflate(R.layout.activity_game_starter_assign, null);
        setContentView(rootView);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sc.setFullView(rootView);
        viewAllPlayersAssign.setGameStarterAssign(this);
        Intent intent=getIntent();
        clientToken=intent.getStringExtra("clienttoken");
        System.out.println(clientToken + " token as in game class");
        key = intent.getStringExtra("key");
        System.out.println(key + " key as in game class");
        userName = intent.getStringExtra("userName");
        System.out.println(userName + " user name as in game class");
        email = intent.getStringExtra("email");
        System.out.println(email + " email as in game class");
        score =intent.getStringExtra("score");
        loggedInKey = intent.getStringExtra("loggedInKey");
        System.out.println(score + " score as in game class");
        playerB = new UserAssign(userName, score, email, clientToken);
        System.out.println(playerB.getEmail() + " player b as received by game class");
        playerB.setKey(key);
        System.out.println(currentlyLoggedInUser.getEmail() + " currently logged in user as received from login page");
        playerA = currentlyLoggedInUser;
        playerA.setKey(loggedInKey);
        System.out.println(playerA.getEmail() + " checking player a's email");
        System.out.println(playerA.getKey() + " checking key of player a");
        //playerB = viewAllPlayersAssign.getPlayerB();
        quitGame = (Button) findViewById(R.id.quitGame);
        superGridMap = spg.setHashMap();
        displayWord = (TextView) findViewById(R.id.displayWord);
        displayScore = (TextView) findViewById(R.id.displayScore);
        displayTime = (TextView) findViewById(R.id.displayTime);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        for(int i = 0; i < gridArray.length; i++)
        {
            final View outer = rootView.findViewById(gridArray[i]);
            sc.setFullView(outer);
            startLetter = spg.selectStartingLetter(superGridMap);
            System.out.println(startLetter + " checking which character is received");
            ArrayList<String> wordsFromLetter = generateRandomWord(startLetter);
            int lengthOfArrayList = wordsFromLetter.size();
            int minIndex = 0, maxIndex = lengthOfArrayList - 1;
            Random rex = new Random();
            int wordKey = rex.nextInt(maxIndex - minIndex + 1) + minIndex;
            String finalWordPerLetter = "";
            finalWordPerLetter = wordsFromLetter.get(wordKey);
            final char[] wordToCharArray = finalWordPerLetter.toCharArray();
            //displayWord.setText("LatestWord: " + finalWordPerLetter);
            final int[] whichPatternArray = ig.addLettersToHashSet(wordToCharArray);
            for(int j = 0; j < cellArray.length; j++)
            {
                //final Button cellButton = (Button) outer.findViewById(cellArray[whichPatternArray[j]]); correct
                final Button cellButton = (Button) outer.findViewById(cellArray[whichPatternArray[j]]);
                final SingleCell cell = smallGrid[i][j];
                final int jTemp = j, iTemp = i;
                char c = (char) wordToCharArray[j];
                final String s = Character.toString(c);
                cellButton.setText(s);

                cellButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        String c = cellButton.getText().toString();
                        isThisLetterClickable = ig.isLetterClickable(whichPatternArray[jTemp]);
                        spg.wordPerGrid[iTemp] += c;
                        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP,150);

                        cellButton.setBackgroundColor(Color.parseColor("#ff0000"));
                        if(spg.wordPerGrid[iTemp].length() >= 3)
                        {
                            boolean wordExistsHuh = readFileForWord(spg.wordPerGrid[iTemp]);
                            if(wordExistsHuh)
                            {

                                Vibrator v = (Vibrator) rootView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
                                // Vibrate for 500 milliseconds
                                v.vibrate(500);
                                recordMove(iTemp, spg.wordPerGrid[iTemp]);
                                displayWord.setText("Your word: " + spg.wordPerGrid[iTemp]);
                                int scoreForThisGrid = ig.pointsPerGrid(spg.wordPerGrid[iTemp], wordExistsHuh);
                                //ig.dynamicScorePerLetter = scoreForThisGrid;
                                //ig.perGridScore += ig.dynamicScorePerLetter;
                                spg.scorePerGrid[iTemp] = scoreForThisGrid;
                                spg.fullScore += spg.scorePerGrid[iTemp];
                                displayScore.setText("Score: " + spg.fullScore);
                                if(spg.scorePerGrid[iTemp] != 0 && spg.scorePerGrid[iTemp] % 100 == 0)
                                    Toast.makeText(getBaseContext(), "Good Job, Keep it going",
                                            Toast.LENGTH_SHORT).show();



                            }
                        }


                       /* }

                        else
                        {

                        }*/

                        System.out.println(wordPerGrid + " check if button is getting clicked");
                    }
                });

            }




        }

        cdt = new CountDownTimer(START, INTERVAL) {
            @Override
            public void onTick(long l) {

                displayTime.setText(""+String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(l)),
                        TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(l))));

                if(l <= 10000) {

                    displayTime.setTextColor(Color.parseColor("#ff0000"));


                }

                if(l <= 5000)
                {
                    Toast.makeText(rootView.getContext(), "Time Remaining:" + TimeUnit.MILLISECONDS.toSeconds(l),
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFinish() {
                Toast.makeText(rootView.getContext(), "Time up, thanks for playing",
                        Toast.LENGTH_SHORT).show();
                for(int i = 0; i < gridArray.length; i++)
                {
                    final View outer = rootView.findViewById(gridArray[i]);
                    sc.setFullView(outer);
                    for(int j = 0; j < cellArray.length; j++)
                    {
                        final Button cellButton = (Button) outer.findViewById(cellArray[j]);
                        cellButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                    }
                    declareWinner(rootView);
                }



            }
        }.start();

        quitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });




    }

    public ArrayList<String> generateRandomWord(char startLetterForFunction)
    {
        String wordForLetter = "";
        Resources resource = getResources();
        String finalWordPerLetter = "";
        ArrayList<String> wordsFromLetter = new ArrayList<String>();
        int fileId = resource.getIdentifier(Character.toString(startLetterForFunction), "raw", getPackageName());
        if(fileId != 0)
        {
            InputStream inputStream = getResources().openRawResource(fileId);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if(inputStream != null)
            {
                try{
                    while((wordForLetter = bufferedReader.readLine()) != null)
                    {
                        wordsFromLetter.add(wordForLetter);
                    }

                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
        return wordsFromLetter;
    }


    public boolean readFileForWord(String word)
    {
        System.out.println(word + " checking if word per grid is sent");
        boolean doesWordExist = false;
        String tempoWord = "";
        Resources resource = getResources();
        ArrayList<String> subSetWith9LetteredWords = new ArrayList<>();
        ArrayList<String> wordsFromLetter = new ArrayList<String>();
        String startLetterForFunction = word.substring(0,3);
        int fileId = resource.getIdentifier(startLetterForFunction, "raw", getPackageName());
        if(fileId != 0)
        {
            InputStream inputStream = getResources().openRawResource(fileId);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if(inputStream != null)
            {
                try{
                    while((tempoWord = bufferedReader.readLine()) != null)
                    {
                        if(tempoWord.length() <= 9)
                            wordsFromLetter.add(tempoWord);
                    }

                    for(String s: wordsFromLetter)
                    {
                        if(s.equals(word))
                        {
                            doesWordExist = true;
                        }
                    }
                }

                catch(IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
        return doesWordExist;
    }

    public void recordMove(int index, String word)
    {
        int wordIndex = index;
        String wordForIndex = word;
        ig.setOwner(currentPlayer);
        if(currentPlayer == IndividualGridAssign.OWNER_OF_TURN.PLAYER_A)
        {
            playerA.setIndexOfWord(wordIndex);
            playerA.setWord(wordForIndex);
        }
        else
        {
            playerB.setIndexOfWord(wordIndex);
            playerB.setWord(wordForIndex);
        }
        switchTurns();
    }

    public void switchTurns()
    {
        currentPlayer = currentPlayer == IndividualGridAssign.OWNER_OF_TURN.PLAYER_A? IndividualGridAssign.OWNER_OF_TURN.PLAYER_B: IndividualGridAssign.OWNER_OF_TURN.PLAYER_A;
    }

    Dialog dialog;
    public void declareWinner(View rootView)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        IndividualGridAssign.OWNER_OF_TURN winnerCandidate = currentPlayer;
        ArrayList<String> wordsByPlayerA = playerA.getWords();
        ArrayList<String> wordsByPlayerB = playerB.getWords();
        boolean isPlayerAValidWord = true;
        boolean isPlayerBValidWord = true;
        ArrayList<Integer> scoreForAllWordsByPlayerA = new ArrayList<>();
        ArrayList<Integer> scoreForAllWordsByPlayerB = new ArrayList<>();
        for(String w: wordsByPlayerA)
        {
            int scoreForThisWord = ig.pointsPerGrid(w, isPlayerAValidWord);
            scoreForAllWordsByPlayerA.add(scoreForThisWord);
        }
        for(String w: wordsByPlayerB)
        {
            int scoreForThisWord = ig.pointsPerGrid(w, isPlayerBValidWord);
            scoreForAllWordsByPlayerB.add(scoreForThisWord);
        }
        int finalScoreForPlayerA = 0, finalScoreForPlayerB = 0;
        for(int s: scoreForAllWordsByPlayerA)
        {
            finalScoreForPlayerA += s;
        }
        for(int s: scoreForAllWordsByPlayerB)
        {
            finalScoreForPlayerB += s;
        }
        playerA.setScore(String.valueOf(finalScoreForPlayerA));
        playerB.setScore(String.valueOf(finalScoreForPlayerB));
        String scoreA = playerA.getScore();
        System.out.println(scoreA + "checking score for player a");
        String scoreB = playerB.getScore();
        mDatabase.child("users").child(playerB.getKey()).child("score").setValue(scoreB);
        mDatabase.child("users").child(playerA.getKey()).child("score").setValue(scoreA);


        if(finalScoreForPlayerA > finalScoreForPlayerB)
        {
            winnerCandidate = IndividualGridAssign.OWNER_OF_TURN.PLAYER_A;
        }
        else if(finalScoreForPlayerB > finalScoreForPlayerA)
        {
            winnerCandidate = IndividualGridAssign.OWNER_OF_TURN.PLAYER_B;
        }
        else
        {
            winnerCandidate = IndividualGridAssign.OWNER_OF_TURN.BOTH;
        }
        IndividualGridAssign.OWNER_OF_TURN winner = winnerCandidate;
        if(winner == IndividualGridAssign.OWNER_OF_TURN.BOTH)
        {
            builder.setMessage("It's a tie!");
        }
        else builder.setMessage(getString(R.string.declare_winner, winner));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok_label,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        dialog = builder.create();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }


        }, 500);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }







}